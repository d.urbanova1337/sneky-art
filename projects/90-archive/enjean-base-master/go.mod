module g

go 1.17

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/fatih/color v1.13.0
	github.com/fsnotify/fsnotify v1.5.1
	github.com/hajimehoshi/ebiten/v2 v2.2.2
	github.com/lucasb-eyer/go-colorful v1.2.0
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410
)

require (
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20211024062804-40e447a793be // indirect
	github.com/jezek/xgb v0.0.0-20210312150743-0e0f116e1240 // indirect
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/exp v0.0.0-20211105205138-14c72366447f // indirect
	golang.org/x/mobile v0.0.0-20211103151657-e68c98865fb2 // indirect
	golang.org/x/mod v0.6.0-dev.0.20211013180041-c96bc1413d57 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20211106132015-ebca88c72f68 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.8-0.20211029000441-d6a9af8af023 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
