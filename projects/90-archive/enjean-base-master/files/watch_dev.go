// +build !js,!production

package files

import (
	"github.com/fsnotify/fsnotify"

	"log"

	. "g/helpers"

	// Might want to live somewhere else?
	"path/filepath"
	"strings"
)

type devWatch struct {
	w   *fsnotify.Watcher
	cbs map[string]func(ChangedFile)
}

func WithWatcher(df *DirFiles) *DirFiles {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		panic(err)
	}

	df.devWatch = devWatch{
		w:   watcher,
		cbs: map[string]func(ChangedFile){},
	}

	go df.waitForEvents()

	return df
}

func (df *DirFiles) Watch(fpath string, cb func(ChangedFile)) {

	err := df.w.Add(df.toabs(fpath))
	if err != nil {
		panic(Err("error adding to watchlist %s: %v", fpath, err))
	}
	df.cbs[fpath] = cb
}

// Because `fsnotify` doesn't have an API for using fs.FS,
// we'll use the absolute path to the files.
func (df *DirFiles) toabs(fpath string) string {
	return filepath.Join(df.AbsDir, fpath)
}

// When we get the event for that file, we'll then
// need to strip the absolute path we added to it.
func (df *DirFiles) fromabs(fpath string) string {

	// FS accepts forward slashes only.
	// Also, we need the leading slash.
	dir := filepath.ToSlash(df.AbsDir) + "/"

	if filepath.IsAbs(fpath) && strings.HasPrefix(fpath, dir) {
		parts := strings.Split(fpath, dir)
		if len(parts) < 2 {
			panic(Err("path might not be from this DirFiles: %v",
				map[string]interface{}{"fpath": fpath, "AbsDir": df.AbsDir}))
		}
		return parts[1]
	} else {
		// NOTE:  This might not be a valid path, it will panic
		//   at the time when opening a file with the path.
		return fpath
	}
}

func (df *DirFiles) waitForEvents() {

	for {

		// BLOCKING!  Always call as a goroutine!

		select {
		case ev := <-df.w.Events:

			if ev.Op != fsnotify.Write {
				log.Printf("Unimportant event:\n   %s %s", ev.Name, ev.Op.String())
				break
			}
			log.Printf("File modified:\n   %s %s", ev.Name, ev.Op.String())

			fpath := df.fromabs(ev.Name)

			bytes, err := df.open(fpath)
			if err != nil {
				log.Printf("[WARN|watch] couldn't read when changed: %v", err)
			} else {

				df.cbs[fpath](ChangedFile{
					Filepath: fpath,
					Bytes:    bytes,
					DF:       df,
				})

			}

		case err := <-df.w.Errors:
			log.Printf("[ERROR|watch] %v", err)
		}
	}
}
