// +build js production

package files

// When doing builds where watching files doesn't make sense,
// we still want the callers to not have to deal with builds.
//
// So, when watch is used in production, it simply does nothing
// and omits any large imports it might have.
//
// Hopefully, any libraries used for doing dev things
// at runtime are also omitted from the build like this.

type (
	Callback    func(ChangedFile)
	ChangedFile struct {
		Filepath string
		Content  []byte
	}
)

func OpenAndWatch(fpath string, cb Callback) {} // noop -- nothing happens.
