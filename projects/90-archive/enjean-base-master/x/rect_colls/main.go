package main

import (
	"g/bang"
	. "g/helpers"

	"image/color"
	"math/rand"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

var (
	colChill     = color.RGBA{150, 200, 10, 200}
	colBang      = color.RGBA{220, 50, 30, 200}
	colContained = color.RGBA{50, 50, 250, 200}
	colContainer = color.RGBA{0, 0, 250, 200}
	colActive    = color.RGBA{200, 200, 200, 200}
)

var rectDefColor = color.Gray{Y: 200}
var activeRecto = NewRect(10, 90, 10, 60)
var staticRectos []RectSprite

type RectSprite struct {
	image       *ebiten.Image
	rect        bang.Rect
	wid, hei    float64
	isContained bool
	isContainer bool
	isBang      bool
}

func NewRect(xmin, xmax, ymin, ymax float64) RectSprite {
	return RectSprite{
		rect:  bang.Rect{Xmin: xmin, Xmax: xmax, Ymin: ymin, Ymax: ymax},
		wid:   xmax - xmin,
		hei:   ymax - ymin,
		image: ebiten.NewImage(int(xmax-xmin), int(ymax-ymin)),
	}
}

func GetRectCenter(r RectSprite) (x, y float64) {
	x = (r.rect.Xmax + r.rect.Xmin) / 2
	y = (r.rect.Ymax + r.rect.Ymin) / 2
	return x, y
}

func NewRectAroundCenter(wid, hei, x, y float64) RectSprite {
	xmin := x - wid/2
	xmax := x + wid/2
	ymin := y - hei/2
	ymax := y + hei/2
	return NewRect(xmin, xmax, ymin, ymax)
}

func PlaceRectAroundCenter(x, y int, r *RectSprite) {
	xmin := float64(x) - r.wid/2
	xmax := float64(x) + r.wid/2
	ymin := float64(y) - r.hei/2
	ymax := float64(y) + r.hei/2
	r.rect.Xmax = xmax
	r.rect.Xmin = xmin
	r.rect.Ymax = ymax
	r.rect.Ymin = ymin
}

// Game implements ebiten.Game interface.
type Game struct{}

// Update proceeds the game state.
// Update is called every tick (1/60 [s] by default).

func (g *Game) Update() error {
	// Write your game's logical update.
	mx, my := ebiten.CursorPosition()
	PlaceRectAroundCenter(mx, my, &activeRecto)
	//fmt.Println(activeRecto.rect.Xmax)

	if inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonLeft) {
		staticRectos = append(staticRectos, activeRecto)
		wid := rand.Float64()*50 + 10
		hei := rand.Float64()*50 + 10
		activeRecto = NewRectAroundCenter(wid, hei, float64(mx), float64(my))
	}
	return nil
}

func ColorBang(r1, r2 RectSprite) {
	isBang := bang.IsBangRect(r1.rect, r2.rect)
	if isBang {
		isContained, container, _ := bang.IsContainedRect(r2.rect, r1.rect)
		if isContained {
			if container == r2.rect {
				r2.image.Fill(colContainer)
				r1.image.Fill(colContained)
			} else {
				r2.image.Fill(colContained)
				r1.image.Fill(colContainer)
			}
		} else {
			r1.image.Fill(colBang)
			r2.image.Fill(colBang)
		}
	} else {
		r1.image.Fill(colChill)
		r2.image.Fill(colActive)
	}
}

// Draw draws the game screen.
// Draw is called every frame (typically 1/60[s] for 60Hz display).
func (g *Game) Draw(screen *ebiten.Image) {
	// Write your game's rendering.
	//screen.Fill(color.RGBA{240, 200, 240, 0xff})
	screen.Fill(color.RGBA{10, 10, 10, 0xff})
	activeRecto.image.Fill(colActive)

	for _, r1 := range staticRectos {
		ColorBang(r1, activeRecto)
		// for _, r2 := range staticRectos {
		// 	ColorBang(r1, r2)
		// }
		op := &ebiten.DrawImageOptions{}
		op.GeoM.Translate(float64(r1.rect.Xmin), float64(r1.rect.Ymin))
		screen.DrawImage(r1.image, op)
	}

	op := &ebiten.DrawImageOptions{}
	op.GeoM.Translate(float64(activeRecto.rect.Xmin), float64(activeRecto.rect.Ymin))

	screen.DrawImage(activeRecto.image, op)

}

// Layout takes the outside size (e.g., the window size) and returns the (logical) screen size.
// If you don't have to adjust the screen size with the outside size, just return a fixed size.
func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return 320, 240
}

func main() {
	game := &Game{}
	Printf("hi there")
	// Specify the window size as you like. Here, a doubled size is specified.
	ebiten.SetWindowSize(640, 480)
	ebiten.SetWindowTitle("Testing Bang")
	// Call ebiten.RunGame to start your game loop.
	if err := ebiten.RunGame(game); err != nil {
		panic(err)
	}
}
