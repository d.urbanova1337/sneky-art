package main

import (
	. "g/helpers"
	"g/klik"

	ebi "github.com/hajimehoshi/ebiten/v2"
	"os"
)

const (
	WIN_TITLE          = "Aseprite Animations in Action!"
	WIN_W              = 1100
	WIN_H              = 700
	DYNAMIC_RESOLUTION = false // see Layout()
)

var (
	a        = struct{ quit klik.Action }{}
	bindings = []klik.KeyBind{{&a.quit, ebi.KeyQ}, {&a.quit, ebi.KeyEscape}}
)

// ---------------------------------------------------------------

type g struct{}

func (g *g) Update() error {
	klik.UpdateControls(bindings, 0)
	if a.quit.IsJustPressed {
		os.Exit(0)
	}

	return nil
}
func (g *g) Draw(s *ebi.Image) {

	return
}

// ---------------------------------------------------------------

func newGame() *g { return &g{} }

func main() {
	ebi.SetWindowSize(WIN_W, WIN_H)
	ebi.SetWindowTitle(WIN_TITLE)
	ebi.SetWindowResizable(true)

	Fmt("Starting: [%s]", WIN_TITLE)

	if err := ebi.RunGame(newGame()); err != nil {
		panic(err)
	}
}
func (g) Layout(outsideW, outsideH int) (w int, h int) {
	if DYNAMIC_RESOLUTION {
		// Resolution follows window size
		return outsideW, outsideH
	} else {
		// Pixels get stretched when we resize
		return WIN_W, WIN_H
	}
}
