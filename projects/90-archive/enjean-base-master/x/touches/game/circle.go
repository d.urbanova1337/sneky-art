package game

import (
	"image"
	"image/color"
	"image/draw"

	. "g/helpers"
)

func makeCircle() (img draw.Image) {
	const r = 30
	img = image.NewRGBA(image.Rect(0, 0, 2*r, 2*r))
	drawCircle(img, r, r, r*1.0, Hsl(0, .5, .5))
	drawCircle(img, r, r, r*0.9, Hsl(0, .5, .5))
	drawCircle(img, r, r, r*0.8, Hsl(0, .5, .5))
	drawCircle(img, r, r, r*0.6, Hsl(0, .5, .5))
	return img
}

func drawCircle(img draw.Image, x0, y0, r int, c color.Color) {
	x, y, dx, dy := r-1, 0, 1, 1
	err := dx - (r * 2)

	for x > y {
		img.Set(x0+x, y0+y, c)
		img.Set(x0+y, y0+x, c)
		img.Set(x0-y, y0+x, c)
		img.Set(x0-x, y0+y, c)
		img.Set(x0-x, y0-y, c)
		img.Set(x0-y, y0-x, c)
		img.Set(x0+y, y0-x, c)
		img.Set(x0+x, y0-y, c)

		if err <= 0 {
			y++
			err += dy
			dy += 2
		}
		if err > 0 {
			x--
			dx += 2
			err += dx - (r * 2)
		}
	}
}
