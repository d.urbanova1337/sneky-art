// +build js production

package main

/*
  GENERATED FILE. You probably shouldn't touch things here.

  [ EMBED MODE ]
    Filesystem embedded into the executable itself for more
    compact production builds.
*/

import (
	df "g/files"

	"embed"
)

var Files = df.NewDirfiles(fsEmbed, []string{
	To_reload_txt,
})

// Filenames listed as constants for code autocompletion
const (
	To_reload_txt = "to_reload.txt"
)

  
//go:embed to_reload.txt
var fsEmbed embed.FS

