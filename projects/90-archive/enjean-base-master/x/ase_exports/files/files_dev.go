// +build !js,!production 

package files

/*
  GENERATED FILE. You probably shouldn't touch things here.

  [ RUNTIME MODE ]
    Filesystem mapped to the real files on disk. These files
    are expected to change.
*/

import (
	df "g/files"

	"os"
	. "g/helpers"
)

var fsReal = os.DirFS(ThisSourceFileDir())

var Files = df.NewDirfiles(fsReal, []string{
	GlitchyNurse_ase,
	GlitchyNurse_json,
	GlitchyNurse_png,
	Plg_nature2_ase,
	Plg_rockyscenery_ase,
	Test_ase,
	Test_json,
	Test_png,
})

// Filenames listed as constants for code autocompletion
const (
    GlitchyNurse_ase = "glitchyNurse.ase"
    GlitchyNurse_json = "glitchyNurse.json"
    GlitchyNurse_png = "glitchyNurse.png"
    Plg_nature2_ase = "plg-nature2.ase"
    Plg_rockyscenery_ase = "plg-rockyscenery.ase"
    Test_ase = "test.ase"
    Test_json = "test.json"
    Test_png = "test.png"
)
