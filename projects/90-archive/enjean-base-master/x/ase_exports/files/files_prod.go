// +build js production

package files

/*
  GENERATED FILE. You probably shouldn't touch things here.

  [ EMBED MODE ]
    Filesystem embedded into the executable itself for more
    compact production builds.
*/

import (
	df "g/files"

	"embed"
)

var Files = df.NewDirfiles(fsEmbed, []string{
	GlitchyNurse_ase,
	GlitchyNurse_json,
	GlitchyNurse_png,
	Plg_nature2_ase,
	Plg_rockyscenery_ase,
	Test_ase,
	Test_json,
	Test_png,
})

// Filenames listed as constants for code autocompletion
const (
	GlitchyNurse_ase = "glitchyNurse.ase"
	GlitchyNurse_json = "glitchyNurse.json"
	GlitchyNurse_png = "glitchyNurse.png"
	Plg_nature2_ase = "plg-nature2.ase"
	Plg_rockyscenery_ase = "plg-rockyscenery.ase"
	Test_ase = "test.ase"
	Test_json = "test.json"
	Test_png = "test.png"
)

  
//go:embed glitchyNurse.ase
//go:embed glitchyNurse.json
//go:embed glitchyNurse.png
//go:embed plg-nature2.ase
//go:embed plg-rockyscenery.ase
//go:embed test.ase
//go:embed test.json
//go:embed test.png
var fsEmbed embed.FS

