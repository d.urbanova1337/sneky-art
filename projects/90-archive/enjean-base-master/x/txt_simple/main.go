package main

import (
	. "g/helpers"
	"g/txt"

	ttf "g/txt/ttf"

	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
)

const (
	WIN_TITLE = "Testing Text"
	WIN_W     = 360
	WIN_H     = 240
)

// ---------------------------------------------------------------

type g struct{}

var loadedFace = txt.GetFaceFile(
	ttf.FantasqueSansMono_Italic_LargeLineHeight_ttf,
	12,
)

func (g) Update() error { return nil }

func (g) Draw(s *ebiten.Image) {

	fps := Fmt("%.2f fps", ebiten.CurrentFPS())

	y := 10
	txt.Dbg(txt.DbgArgs{
		DestImage: s,
		Text:      fps + " - Printed with Debug Printing.",
		X:         10,
		Y:         y,
	})
	txt.Draw(txt.DrawArgs{
		DestImage: s,
		Text:      fps + " - Printed with a \"pretty\" TTF Font.",
		X:         10,
		Y:         y,
		Color:     color.White,
		Face:      txt.GetFace(12, txt.MonoLight),
	})

	//

	line := txt.DrawArgs{
		DestImage: s, Text: "later",
		X:     14,
		Y:     50,
		Color: color.Gray{180}, Face: loadedFace, //txt.Face(16, txt.MonoBold),
	}
	line.Text = "As you can see, even though the Y value"
	txt.Draw(line)

	line.Y += line.Face.Metrics().Height.Ceil()
	line.Text = Fmt("on both is %d, Dbg and Draw are aligned", y)
	txt.Draw(line)

	line.Y += line.Face.Metrics().Height.Ceil()
	line.Text = "differently."
	txt.Draw(line)

	// TODO:  Make a text-block function.

	return
}

// ---------------------------------------------------------------

func main() {
	ebiten.SetWindowSize(WIN_W, WIN_H)
	ebiten.SetWindowTitle(WIN_TITLE)
	ebiten.SetWindowResizable(true)
	if err := ebiten.RunGame(g{}); err != nil {
		panic(err)
	}
}
func (g) Layout(outsideW, outsideH int) (w int, h int) {
	return WIN_W, WIN_H
}
