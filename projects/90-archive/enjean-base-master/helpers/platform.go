package helpers

import (
	"path/filepath"
	"runtime"
)

// This is how we can figure out if we are in a WebBuild or not.
// By the way, this won't tell us if we're in a production build,
// only what "OS" we're running on.
const WebBuild = runtime.GOOS == "js"

//

// The caller function wants us to pass in the stack depth it should look at.
// 0 is this function's body right here (not useful),
// 1 is this function's caller
// ...and so on...
func SourceFileAtStackDepth(depth int) string {

	_, fpath, _, _ := runtime.Caller(depth)

	dir := filepath.Dir(fpath)

	if abs, err := filepath.Abs(dir); err != nil {
		// Finding out what absolute path something is can fail... huh, aight.
		return dir
	} else {
		return abs
	}
}

// ThisSourceFileDir returns the directory of the source file
// this function is called in.
//
// I.e; To find out what directory is the file you're currently
// writing code inside, use this function.
//
func ThisSourceFileDir() string {

	// Because we want to get where this is called from (+1),
	// and because this is a wrapper function (+1)
	// we need to pass in a 2(two).

	return SourceFileAtStackDepth(2)
}

// CallerSourceFileDir returns the directory of the source file
// that calls the function that calls this function.
//
// I.e; To find out in your code who called the function
// you're currently in, use this function.
//
func CallerSourceFileDir() string {

	// This function is providing the caller to get the caller's
	// source file. We need to go up 3(three) levels:
	//  - From add a level for the wrapper   (unwrap)
	//  - From this function to its caller   (out of the lib)
	//  - From the caller to _its_ caller    (out of the caller)

	return SourceFileAtStackDepth(3)

	// ...It makes sense when you see the usage, othewise
	// it's very meta to think about.
}
