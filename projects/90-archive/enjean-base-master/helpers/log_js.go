// +build js

package helpers

import (
	"log"
	"strings"
	"time"

	"fmt"
)

func init() {
	// Default std `log` setup
	log.SetFlags(0) // Disabling builtin timestamp and other decor
	log.SetOutput(new(webWriter))
	log.Printf("Web logging here.")
}

// NOTE:  We don't want the huge spewing library necessary for
//   the web build, we can dump the old way instead.
//   Less informative but should be fine in case we need
//   to debug something in a browser.

func Doomp(o interface{}) string { return fmt.Sprintf("%#v", o) }

//
// -- WEB --
//

// Logging to a console in a browser is ridiculously slow for logging
// anything every frame in a game. Just don't do it.
//
// TODO:  Instead of throwing out the logs, make a log HTML element
//   which should be faster to write to.

// TODO:  Check our panic recovery if logs in the browser; @errors.go.

type webWriter struct{}

func (webWriter) Write(bytes []byte) (int, error) {
	return len(bytes), nil
}
