// +build !js

package helpers

//
//
// TODO:  Look up how we do child logging in `planeo-ws` and steal
//   that code into here. I want my prefixes.
//
//

import (
	"log"
	"strings"
	"time"

	C "github.com/fatih/color"

	"fmt"

	"github.com/davecgh/go-spew/spew"
)

var (
	// https://github.com/davecgh/go-spew#sample-formatter-output
	Doomp = spew.Sdump // alias

	Printf = log.Printf
)

//
// -- DESKTOP --
//
// Pretty logging through the standard logging mechanism.

// TODO:  Add a configurable bool that will enable showing where
//   the log is being called from. (@conf)

func init() {

	spew.Config.Indent = "  " // one space is a bit too little imo

	// Disabling builtin timestamp and any other decor.
	log.SetFlags(0)
	// Making the writing pretty by managing the writing ourselves.
	log.SetOutput(new(prettyWriter))
}

type prettyWriter struct{}

func (prettyWriter) Write(bytes []byte) (int, error) {
	var timestamp string
	{ // Use a timestamp prefix
		now := time.Now()
		minuteSecond := now.Format("04'05\"")
		millis := now.Format(".999")

		// Align milliseconds, pad with zeroes on the right
		if millis != "" { // Can just be empty if we have exact seconds
			millis = millis[1:] // Don't include the dot
		}
		millis = millis + strings.Repeat("0", 3-len(millis))

		const WITH_COLOR = true
		if WITH_COLOR {
			timestamp = C.YellowString("%s%s ║ ", minuteSecond, millis)
		} else {
			timestamp = fmt.Sprintf("%s%s ║ ", minuteSecond, millis)
		}
	}
	return fmt.Print(timestamp + string(bytes))
}
