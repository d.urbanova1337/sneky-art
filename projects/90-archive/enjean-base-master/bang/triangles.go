package bang

/* ==========================================
def line_intersect2(v1,v2,v3,v4):
    '''
    judge if line (v1,v2) intersects with line(v3,v4)
    '''
    d = (v4[1]-v3[1])*(v2[0]-v1[0])-(v4[0]-v3[0])*(v2[1]-v1[1])
    u = (v4[0]-v3[0])*(v1[1]-v3[1])-(v4[1]-v3[1])*(v1[0]-v3[0])
    v = (v2[0]-v1[0])*(v1[1]-v3[1])-(v2[1]-v1[1])*(v1[0]-v3[0])
    if d<0:
        u,v,d= -u,-v,-d
    return (0<=u<=d) and (0<=v<=d)
*/

func LinesIntersect(a, b, s, t [2]float64) bool {
	d := (t[1]-s[1])*(b[0]-a[0]) - (t[0]-s[0])*(b[1]-a[1])
	u := (t[0]-s[0])*(a[1]-s[1]) - (t[1]-s[1])*(a[0]-s[0])
	v := (b[0]-a[0])*(a[1]-s[1]) - (b[1]-a[1])*(a[0]-s[0])
	if d < 0 {
		u, v, d = -u, -v, -d
	}
	return (0 <= u && u <= d) && (0 <= v && v <= d)
}

/* ======================================
def point_in_triangle2(A,B,C,P):
    v0 = [C[0]-A[0], C[1]-A[1]]
    v1 = [B[0]-A[0], B[1]-A[1]]
    v2 = [P[0]-A[0], P[1]-A[1]]
    cross = lambda u,v: u[0]*v[1]-u[1]*v[0]
    u = cross(v2,v0)
    v = cross(v1,v2)
    d = cross(v1,v0)
    if d<0:
        u,v,d = -u,-v,-d
    return u>=0 and v>=0 and (u+v) <= d
*/

func PointInTriangle(a, b, c, p [2]float64) bool {
	v0 := [2]float64{c[0] - a[0], c[1] - a[1]}
	v1 := [2]float64{b[0] - a[0], b[1] - a[1]}
	v2 := [2]float64{p[0] - a[0], p[1] - a[1]}
	u := crossProduct(v2, v0)
	v := crossProduct(v1, v2)
	d := crossProduct(v1, v0)
	if d < 0 {
		u, v, d = -u, -v, -d
	}
	return u >= 0 && v >= 0 && (u+v) <= d
}

func crossProduct(u, v [2]float64) float64 {
	return u[0]*v[1] - u[1]*v[0]
}

/* ==================================
def tri_intersect2(t1, t2):
    '''
    judge if two triangles in a plane intersect
    '''
    if line_intersect2(t1[0],t1[1],t2[0],t2[1]): return True
    if line_intersect2(t1[0],t1[1],t2[0],t2[2]): return True
    if line_intersect2(t1[0],t1[1],t2[1],t2[2]): return True
    if line_intersect2(t1[0],t1[2],t2[0],t2[1]): return True
    if line_intersect2(t1[0],t1[2],t2[0],t2[2]): return True
    if line_intersect2(t1[0],t1[2],t2[1],t2[2]): return True
    if line_intersect2(t1[1],t1[2],t2[0],t2[1]): return True
    if line_intersect2(t1[1],t1[2],t2[0],t2[2]): return True
    if line_intersect2(t1[1],t1[2],t2[1],t2[2]): return True
    inTri = True
    inTri = inTri and point_in_triangle2(t1[0],t1[1],t1[2], t2[0])
    inTri = inTri and point_in_triangle2(t1[0],t1[1],t1[2], t2[1])
    inTri = inTri and point_in_triangle2(t1[0],t1[1],t1[2], t2[2])
    if inTri == True: return True
    inTri = True
    inTri = inTri and point_in_triangle2(t2[0],t2[1],t2[2], t1[0])
    inTri = inTri and point_in_triangle2(t2[0],t2[1],t2[2], t1[1])
    inTri = inTri and point_in_triangle2(t2[0],t2[1],t2[2], t1[2])
    if inTri == True: return True
    return False
*/

func TrianglesIntersect(t1, t2 [3][2]float64) bool {

	switch {
	case LinesIntersect(t1[0], t1[1], t2[0], t2[1]):
		return true
	case LinesIntersect(t1[0], t1[1], t2[0], t2[2]):
		return true
	case LinesIntersect(t1[0], t1[1], t2[1], t2[2]):
		return true
	case LinesIntersect(t1[0], t1[2], t2[0], t2[1]):
		return true
	case LinesIntersect(t1[0], t1[2], t2[0], t2[2]):
		return true
	case LinesIntersect(t1[0], t1[2], t2[1], t2[2]):
		return true
	case LinesIntersect(t1[1], t1[2], t2[0], t2[1]):
		return true
	case LinesIntersect(t1[1], t1[2], t2[0], t2[2]):
		return true
	case LinesIntersect(t1[1], t1[2], t2[1], t2[2]):
		return true
	default:
		// do nothing
	}

	{
		inside := true
		inside = inside && PointInTriangle(t1[0], t1[1], t1[2], t2[0])
		inside = inside && PointInTriangle(t1[0], t1[1], t1[2], t2[1])
		inside = inside && PointInTriangle(t1[0], t1[1], t1[2], t2[2])
		if inside {
			return true
		}
	}
	{
		inside := true
		inside = inside && PointInTriangle(t2[0], t2[1], t2[2], t1[0])
		inside = inside && PointInTriangle(t2[0], t2[1], t2[2], t1[1])
		inside = inside && PointInTriangle(t2[0], t2[1], t2[2], t1[2])
		if inside {
			return true
		}
	}
	return false
}
