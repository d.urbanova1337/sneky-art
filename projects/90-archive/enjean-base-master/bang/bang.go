package bang

// here go all the functions

type Rect struct {
	Xmin, Xmax, Ymin, Ymax float64
}

var startingRects = []Rect{}

type Circle struct {
	x, y float64
	rad  float64
}

func IsBangCircles(a, b Circle) {

}

func IsBang(x1, x2 int) bool {
	if x1 == x2 {
		return true
	} else {
		return false
	}
}

// Fourangles
func IsBangRect(a, b Rect) bool {
	xBang := false
	yBang := false

	//		aMin == bMin || aMax == bMax ||
	//		aMin == bMax || aMax == bMin )
	// this is included in the <>= block below

	// 		(aMin <= bMin && aMin >= bMax)
	// this is bullshit

	if (a.Xmin >= b.Xmin && a.Xmin <= b.Xmax) || (b.Xmin >= a.Xmin && b.Xmin <= a.Xmax) {
		xBang = true
	}
	if (a.Ymin >= b.Ymin && a.Ymin <= b.Ymax) || (b.Ymin >= a.Ymin && b.Ymin <= a.Ymax) {
		yBang = true
	}

	return xBang && yBang
}

func IsContainedRect(a, b Rect) (isCont bool, container Rect, contained Rect) {
	if (a.Xmin <= b.Xmin && a.Xmax >= b.Xmax) && (a.Ymin <= b.Ymin && a.Ymax >= b.Ymax) {
		return true, a, b
	}
	if (b.Xmin <= a.Xmin && b.Xmax >= a.Xmax) && (b.Ymin <= a.Ymin && b.Ymax >= a.Ymax) {
		return true, b, a
	}
	return false, Rect{}, Rect{}
}
