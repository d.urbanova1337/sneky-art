// +build !js,!production 

package ttf

/*
  GENERATED FILE. You probably shouldn't touch things here.

  [ RUNTIME MODE ]
    Filesystem mapped to the real files on disk. These files
    are expected to change.
*/

import (
	df "g/files"

	"os"
	. "g/helpers"
)

var fsReal = os.DirFS(ThisSourceFileDir())

var Files = df.NewDirfiles(fsReal, []string{
	FantasqueSansMono_Bold_ttf,
	FantasqueSansMono_Bold_LargeLineHeight_ttf,
	FantasqueSansMono_Italic_ttf,
	FantasqueSansMono_Italic_LargeLineHeight_ttf,
	FantasqueSansMono_Regular_ttf,
	FantasqueSansMono_Regular_LargeLineHeight_ttf,
	Inconsolata_ExtraCondensed_Bold_ttf,
	Inconsolata_ExtraCondensed_Light_ttf,
	Inconsolata_ExtraCondensed_Regular_ttf,
})

// Filenames listed as constants for code autocompletion
const (
    FantasqueSansMono_Bold_ttf = "FantasqueSansMono-Bold.ttf"
    FantasqueSansMono_Bold_LargeLineHeight_ttf = "FantasqueSansMono-Bold_LargeLineHeight.ttf"
    FantasqueSansMono_Italic_ttf = "FantasqueSansMono-Italic.ttf"
    FantasqueSansMono_Italic_LargeLineHeight_ttf = "FantasqueSansMono-Italic_LargeLineHeight.ttf"
    FantasqueSansMono_Regular_ttf = "FantasqueSansMono-Regular.ttf"
    FantasqueSansMono_Regular_LargeLineHeight_ttf = "FantasqueSansMono-Regular_LargeLineHeight.ttf"
    Inconsolata_ExtraCondensed_Bold_ttf = "Inconsolata_ExtraCondensed-Bold.ttf"
    Inconsolata_ExtraCondensed_Light_ttf = "Inconsolata_ExtraCondensed-Light.ttf"
    Inconsolata_ExtraCondensed_Regular_ttf = "Inconsolata_ExtraCondensed-Regular.ttf"
)
