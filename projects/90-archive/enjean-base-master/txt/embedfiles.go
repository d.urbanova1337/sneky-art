package txt

import _ "embed"

//go:embed ttf/Inconsolata_ExtraCondensed-Light.ttf
var monoLight []byte
var MonoLight = &monoLight

//go:embed ttf/Inconsolata_ExtraCondensed-Regular.ttf
var monoReg []byte
var MonoReg = &monoReg

//go:embed ttf/Inconsolata_ExtraCondensed-Bold.ttf
var monoBold []byte
var MonoBold = &monoBold
