package anim

import (
	"log"
	"strings"

	ebi "github.com/hajimehoshi/ebiten/v2"

	. "g/helpers"
)

type Playback struct {
	ID

	// Controlled animation data, can (and should) be shared.
	// It's __embedded__ so we can use its methods directly without mentioning
	// that it's the animation's method. I mean sometimes it's useful,
	// but most of the time I just want to find a method quickly.
	*Data

	//

	// FrameIndex points to a frame in an animation.
	FrameIndex int

	// DurationCounter is for counting time until the next frame.
	// It's reset every frame, it doesn't count total time played or anything.
	DurationCounter Secs

	//

	// One animation can have multiple layers, so we have to specify which
	// one we want to play.
	// Layer corresponds with multiple alternatives of the same Tag.
	AnimationLayer string

	// Tag is a slice of frames in an animation.
	playedTag     Tag
	playedTagName string

	// Action to take when at the end of a Tag.
	mode Mode
}

//
//  Playback control
//

type Mode int

const (
	PLAYBACK_AT_END__LOOP Mode = iota
	PLAYBACK_AT_END__STOP
	PLAYBACK_AT_END__NOTHING // Continues through the tag -- kinda dangerous
	PLAYBACK_PAUSE           // Stops the playback / prevents advancing the animation
)

// If past the end, loop back to the start.
func (pl *Playback) AtEnd_Loop() *Playback {
	pl.mode = PLAYBACK_AT_END__LOOP
	return pl
}

// If past the end, stop at the end.
func (pl *Playback) AtEnd_Stop() *Playback {
	pl.mode = PLAYBACK_AT_END__STOP
	return pl
}

//

// Get the current sprite, as defined by the current frame of the player
func (pl Playback) Current_Sprite() *ebi.Image {
	return pl.Current_Frame().SubImage
}

// Get the current frame, as defined by player's timers and played tag
func (pl Playback) Current_Frame() Frame {
	return pl.FrameByIndex(pl.FrameIndex)
}

//

// Get a frame by its index in the currently played tag
func (pl Playback) FrameByIndex(frameIndex int) Frame {
	return pl.playedTag.Frames[frameIndex]
}

// CompletedFrames_01 returns the completion percentage by frame count.
// This is faster to check than checking time, but almost never what
// we actually want.
func (pl Playback) CompletedFrames_01() float64 {
	return float64(pl.FrameIndex) / float64(len(pl.playedTag.Frames)-1)
}

// Completed_01 returns the completion percentage by time expired
// in the animation currently played.
func (pl Playback) Completed_01() float64 {
	return pl.DurationCounter / pl.playedTag.TotalDuration()
}

const ANIMATION_TAG_DEFAULT = "default"

// Switch to the default aseprite tag, playing the full animation.
func (pl *Playback) Default_Tag() *Playback {
	pl.playedTag = pl.Data.TagDefault()
	pl.playedTagName = ANIMATION_TAG_DEFAULT
	return pl
}
func (pl *Playback) Set_Layer(layerName string) *Playback {
	pl.AnimationLayer = layerName
	// TODO:  Validate
	return pl
}
func (pl *Playback) Set_Tag(tagName string) *Playback {

	if pl.AnimationLayer != "" {
		// Prepend the tagName with the animation layer only
		// if that hasn't happened already. Might be "just probing".
		layerPrefix := pl.AnimationLayer + "/" // @layertag
		if !strings.HasPrefix(tagName, layerPrefix) {
			tagName = layerPrefix + tagName
		}
	}

	tag, isValidTag := pl.Data.TagByName(tagName)
	if !isValidTag {
		if !WebBuild { //@noreload
			_, stillExists := pl.Data.TagByName(pl.playedTagName)
			if !stillExists {
				log.Printf(
					"And the tag we had isn't there anymore, switching to default")
				pl.Default_Tag()
			}
		}
		log.Printf("[ERR] Tagswitch #%s; ignoring...\n", tagName)
		return pl
	}

	pl.playedTag = tag
	pl.playedTagName = tagName

	return pl
}

func (pl Playback) Current_Tag() Tag { return pl.playedTag }

// Restart the animation; reset the frame index.
func (pl *Playback) Restart() *Playback {
	pl.FrameIndex = 0
	pl.DurationCounter = 0
	return pl
}

// Advance the animation by one frame unconditionally.
func (pl *Playback) Advance_Frame() *Playback {
	if pl.mode == PLAYBACK_PAUSE {
		return pl
	}
	pl.FrameIndex++
	pl.DurationCounter = 0
	return pl
}

// Advance the animation by a time duration, respecting the imported
// animation's frame durations; Supports going into negative numbers.
// NOTE:  Does not support skipping multiple frames in one call.
func (pl *Playback) Advance_Secs(dt Secs) *Playback {

	// Some animations aren't actually animations
	if pl.mode == PLAYBACK_PAUSE {
		return pl
	}

	// TODO:  Introduce some "noreload" build tag or something :: @noreload
	if !WebBuild {
		pl.Set_Tag(pl.playedTagName)
	}

	if dt > 0 {
		pl.DurationCounter += dt

		var nextSwitch = pl.Current_Frame().DurationSecs
		if pl.DurationCounter >= nextSwitch {
			// Careful about carrying the overflow into the next frame
			pl.DurationCounter -= nextSwitch
			pl.FrameIndex++
		}
	} else {
		pl.DurationCounter += dt // will be negative so it's the same

		// Next switch is trivial
		if pl.DurationCounter < 0 {
			// Leave the right overflow when going backwards
			var prevSwitch = pl.FrameByIndex(pl.FrameIndex).DurationSecs
			pl.DurationCounter += prevSwitch
			pl.FrameIndex--
		}
	}

	{ // What to do if we're at the end of an animation
		var (
			high = len(pl.playedTag.Frames) - 1
			low  = 0
		)
		switch pl.mode {
		case PLAYBACK_AT_END__NOTHING:
			break
		case PLAYBACK_AT_END__LOOP:
			if pl.FrameIndex > high {
				pl.FrameIndex = low
			}
			if pl.FrameIndex < low {
				pl.FrameIndex = high
			}
		case PLAYBACK_AT_END__STOP:
			if pl.FrameIndex > high {
				pl.FrameIndex = high
			}
			if pl.FrameIndex < low {
				pl.FrameIndex = low
			}
		}
	}

	// Note: Using `Next()` directly can overflow
	return pl
}
