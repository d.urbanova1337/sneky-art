// +build !js,!production

package anim

import (
	"g/files"

	. "g/helpers"

	"path/filepath"
	"strings"

	"bytes"
	"encoding/json"
	"image/png"
	"os/exec"
	"time"

	ebi "github.com/hajimehoshi/ebiten/v2"
)

func New(df *files.DirFiles, fpath string) *Data {
	// NOTE:  Filepath extension doesn't matter.

	data := &Data{}

	// We won't be watching the assets themselves, those will be modified
	// by us directly -- we ourselves will be triggering the exports!
	df.Watch(base(fpath)+extASE, data.doExport)

	var (
		// We want to re-export on game load, if the .ase file is newer
		// than the .json/.png files generated from the file.
		// NOTE:  Our `DirFiles.Modtime` returns a zero-modtime on error,
		//   in which case export always happens. This includes missing
		//   files and other errors.
		base        = base(fpath)
		modPng      = df.Modtime(base + extPNG)
		modJson     = df.Modtime(base + extJSON)
		modAse      = df.Modtime(base + extASE)
		needsExport = modAse.After(modJson) || modAse.After(modPng)
	)
	if needsExport {
		go data.doExport(files.ChangedFile{
			Filepath: fpath,    // will do its own base()
			Bytes:    []byte{}, // ! we don't need the contents
			DF:       df,       // will be necessary to get absolute folder path
		})
	} else {
		go data.loadData(df, base)
	}

	return data
}

//

func (data *Data) loadData(df *files.DirFiles, base string) {
	tDiff := time.Now()

	var jsonBytes, pngBytes []byte
	// Reading the just-generated data

	// TODO:  This panics, not good!
	df.Open(base+extJSON, func(f files.ChangedFile) { jsonBytes = f.Bytes })
	df.Open(base+extPNG, func(f files.ChangedFile) { pngBytes = f.Bytes })

	Printf("Both files read (%s):\n  JSON: %.3fkb\n  PNG:  %.3fkb",
		time.Since(tDiff),
		float64(len(jsonBytes))/1024, float64(len(pngBytes))/1024)
	tDiff = time.Now()

	var (
		sheet *ebi.Image
		tags  map[string]Tag
	)
	{ // sheet
		img, err := png.Decode(bytes.NewReader(pngBytes))
		if err != nil {
			Printf("[ERR] Sheet not a png? %v", err)
			return
		}
		sheet = ebi.NewImageFromImage(img)

		Printf("Decoded the sheet (%s)", time.Since(tDiff))
		tDiff = time.Now()
	}
	{ // tags
		var ase aseJSON
		err := json.Unmarshal(jsonBytes, &ase)
		if err != nil {
			Printf("[ERR] JSON error? %v", err)
			return
		}
		tags = defineTags(ase, sheet)

		Printf("Made the tags as the JSON says (%s)", time.Since(tDiff))
		tDiff = time.Now()
	}

	// TODO:  A lock to where we aren't drawing would be handy!

	data.Sheet = sheet
	data.Tags = tags

	Printf("Data updated for %s.", base)

	// TODO:  We need to let any players that use this animation data
	//   reconcile the changes that happened to its data.
	//   It should be possible to at least reset and set fallbacks
	//   to the whole animation.
}

//

func cmd(df *files.DirFiles, base string) (*exec.Cmd, []string) {

	filebase := filepath.Join(df.AbsDir, base)

	modifier := StringEnclosedIn(base, "(", ")")
	if modifier != "" {
		Printf("TODO:  Modifiers are not implemented. Aseprite is always called the same way")
	}

	// TODO:

	fields := []string{
		// `aseprite` must be installed and in PATH, obviously
		"aseprite",

		// Options set to minify the output somewhat, while being lossless.
		"--color-mode", "indexed",
		"--sheet-pack",
		"--trim",
		// TODO:  I'm not sure about this one... used to be buggy
		"--ignore-empty",

		// We always ignore layers with this as its name. This is very
		// useful for backgrounds that you won't be hiding every time
		// you want to save & export a thing.
		"--ignore-layer", "_",

		// These are always the same, cannot be modified
		"--batch",                // Tells `aseprite` to not run UI
		"--format", "json-array", // Has to be like this for our JSON decoding!
		"--sheet", filebase + extPNG,
		"--data", filebase + extJSON,
		filebase + extASE,
	}

	cmd := exec.Command(fields[0], fields[1:]...)
	return cmd, fields
}

func (data *Data) doExport(f files.ChangedFile) {

	tStart := time.Now()
	tDiff := tStart
	// NOTE(perf):  For small files, these timings look like 95% being
	//   running aseprite (~100ms). It doesn't look like there's much point
	//   in optimizing our end -- it's not what's making things slow.
	//   The best we could do would be finding some exporter made in Rust
	//   that would do aseprite's job but better. Unlikely!

	base := base(f.Filepath)

	{ // Running aseprite
		cmd, fields := cmd(f.DF, base)

		Printf("Exporting %s\n> %v\n\n", f.Filepath, strings.Join(fields, "\n  "))

		asepriteOutput, err := cmd.Output()
		if err != nil {
			// NOTE:  We'll get an error every time the exit code is non-zero.
			// TODO:  Test it though!
			// TODO:  Test where we get the error message -- err or output
			Printf("[ERR] aseprite command error: \n ERR: %v\n OUTPUT: %v",
				err, asepriteOutput)
			// TODO:  .LastCommand maybe shouldn't be commited if it
			//   actually returned an error? I'm not sure...
			return
		}

		// At this point, we should have all the files we need to load in the asset...
		Printf("Aseprite command ran OK. (%s)", time.Since(tDiff))
		tDiff = time.Now()
	}

	// Loading the data after we're for sure successfully finished exporting.
	data.loadData(f.DF, base)
}

//

func base(fpath string) string { return strings.Split(fpath, filepath.Ext(fpath))[0] }

const (
	extJSON = ".json"
	extPNG  = ".png"
	extASE  = ".ase"
)
