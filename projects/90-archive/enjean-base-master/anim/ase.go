package anim

import (
	ebi "github.com/hajimehoshi/ebiten/v2"

	. "g/helpers"

	"image"
	"strconv"
)

/*

   The `ase.go` describes reading data from `.json` files
   that can be exported alongside the `.png` files.

   These JSON files describe what the image contains when various
   atlasing and compacting techniques are used and how to construct
   animations defined in Aseprite from the spritesheet.

   ---

   The file is responsible only for loading information out of the files,
   and it's up to the animation or whatever system to take the information
   out and reformat it to its needs.

   TLDR; This is just the loader.

   ---

   For the most part, we're not renaming the fields by defining struct tags
   (`json:"fieldname_in_file"`) and instead we're using names directly.

   Only exception is when we're using the field in our own way and we want
   to reinterpret the field in the code directly, using tags to show intent.
   Sometimes the original names are just confusing with our primary usage.

*/

// TODO:  Read triggers from the animation (sounds, events...)
// TODO:  Read positions from single-pixel layers somehow
// TODO:  Read aseprite slices for atlas-based tilesystems

type (
	aseJSON struct {
		Meta   aseMeta    // File properties, sizes and individual tags
		Frames []aseFrame // Frame bounds and durations
	}
	aseMeta struct {
		Image     string        // Filename of the spritesheet image
		Size      aseSize       // Size of the spritesheet
		FrameTags []aseFrameTag // Individual animations
		Layers    []aseLayer
	}
	aseFrameTag struct {
		Name string
		From int
		To   int
	}
	aseFrame struct {
		// Can be customized to keep track of a frame's layer, slice, tag...
		CustomizableName string `json:"filename"`

		DurationMillis int     `json:"duration"` // Duration in ms
		CutoutRect     aseRect `json:"frame"`    // Spritesheet's cutout region

		Trimmed, Rotated bool // ..unused
		SpriteSourceSize aseRect
		SourceSize       aseSize
	}
	aseLayer struct {
		Name  string // Name of the node
		Group string // Parent node's `.Name`
		// NOTE:  If the layer is a folder, it will only have a name

		Opacity255 int
		BlendMode  string
	}

	aseRect struct {
		X, Y, W, H int
	}
	aseSize struct {
		W, H int
	}
)

func defineTags(ase aseJSON, spritesheet *ebi.Image) map[string]Tag {

	tags := make(map[string]Tag)

	// Always include the full animation as a default tag
	tags[ANIMATION_TAG_DEFAULT] = Tag{
		Name:   ANIMATION_TAG_DEFAULT,
		Frames: defineFrames(spritesheet, ase.Frames[:]),
	}

	if len(ase.Meta.Layers) == 0 {
		// Add the individual frame tags set in Aseprite
		for _, t := range ase.Meta.FrameTags {
			fr := ase.Frames[t.From : t.To+1]
			tags[t.Name] = Tag{
				Name:   t.Name,
				Frames: defineFrames(spritesheet, fr),
			}
		}
	} else {
		// We have to think about all the layers here...
		// Prefix each tag from every layer by that layer's name
		for _, lay := range ase.Meta.Layers {
			layerTags := defineTagsLayer(ase, spritesheet, lay.Name)
			for tagName, frames := range layerTags {
				layerPrefix := lay.Name + "/" // @layertag
				tags[layerPrefix+tagName] = frames
			}
		}
	}
	return tags
}
func defineTagsLayer(ase aseJSON, spritesheet *ebi.Image, layer string) map[string]Tag {
	Assert(layer != "", "no layer supplied for tag definition")
	// NOTE:  When we throw layers into the mix, we'll have to grab data
	//   from the filename -- meta no longer helps as it's a multidimensional
	//   problem that requires more than just one array of frames to decode.

	// Collect the tags' aseframe data first...
	tagFrames := make(map[string][]aseFrame)
	add := func(tagName string, frame aseFrame) { //@closure(tagFrames)
		frameList, ok := tagFrames[tagName]
		if !ok { // Make the tag so we can add to it
			frameList = make([]aseFrame, 0)
		}
		frameList = append(frameList, frame)
		tagFrames[tagName] = frameList // Don't forget to apply changes back
	}

	for _, fr := range ase.Frames {
		tagName, layerName, _ := fr.belongsTo()
		if layer != layerName {
			continue // skip
		}
		add(tagName, fr)
		add(ANIMATION_TAG_DEFAULT, fr)
	}

	// Then process them into the spritesheet regions...
	tags := make(map[string]Tag)
	for tagName, frameList := range tagFrames {
		tags[tagName] = Tag{
			Name:   tagName,
			Frames: defineFrames(spritesheet, frameList),
		}
	}
	return tags
}

func defineFrames(sheet *ebi.Image, aseFrames []aseFrame) []Frame {
	var frames = make([]Frame, len(aseFrames))
	for index, it := range aseFrames {
		frames[index] = Frame{
			SubImage:     sheet.SubImage(it.CutoutRect.toStd()).(*ebi.Image),
			DurationSecs: float64(it.DurationMillis) / 1000,
		}
	}
	return frames
}

func (frame aseFrame) belongsTo() (tag string, layer string, frameIndex int) {

	layer = StringEnclosedIn(frame.CustomizableName, "(", ")")
	tag = StringEnclosedIn(frame.CustomizableName, "#", "#")

	frameIndex = 0
	// Defaults to zero, we might not get an index when there's only
	// one frame in a tag.
	{
		frameIndexStr := StringEnclosedIn(frame.CustomizableName, "[", "]")
		if frameIndexStr != "" {
			int32, err := strconv.ParseInt(frameIndexStr, 10, 32)
			if err == nil {
				frameIndex = int(int32)
			}
		}
	}
	return
}
func (meta aseMeta) layerStructure() map[string]map[string]aseLayer {

	// NOTE:  The layers are an ordered list that stays in the same
	//   order as when you're looking at the layers in aseprite;
	//   the folder is first, then its contents... With that, we'll
	//   be able to collect everything in one go, as child nodes
	//   are always preceded with their parent.
	// NOTE:  I'm not sure how multi-level folders look so I'm not gonna
	//   do those just yet -- this is for simple folder*/layer* structure.

	m := make(map[string]map[string]aseLayer)
	for _, l := range meta.Layers {
		if len(l.Group) == 0 {
			m[l.Name] = make(map[string]aseLayer)
		} else {
			m[l.Group][l.Name] = l
		}
	}
	return m
}

//

func (r aseRect) toStd() image.Rectangle { return image.Rect(r.X, r.Y, r.X+r.W, r.Y+r.H) }
