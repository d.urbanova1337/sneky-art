package klik

import (
	"github.com/hajimehoshi/ebiten/v2"
)

type KeyBind struct {
	ActionPtr *Action
	Key       ebiten.Key
}

func (als Aliases) CheckKeybinds(keys []KeyBind) {
	for _, bind := range keys {
		als.Add(bind.ActionPtr, ebiten.IsKeyPressed(bind.Key))
	}
}
