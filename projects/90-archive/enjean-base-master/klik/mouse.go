package klik

import (
	"github.com/hajimehoshi/ebiten/v2"
)

type MouseBind struct {
	ActionPtr   *Action
	MouseButton ebiten.MouseButton
}

func (als Aliases) CheckMousebinds(binds []MouseBind) {
	for _, bind := range binds {
		als.Add(bind.ActionPtr, ebiten.IsMouseButtonPressed(bind.MouseButton))
	}
}

//

var Mouse = mouseManager{}

type mouseManager struct {
	ScreenPos
	ClickLeft, ClickRight, ClickMiddle Action
}

//

func (m *mouseManager) defaultBinds() []MouseBind {
	return []MouseBind{
		{&m.ClickLeft, ebiten.MouseButtonLeft},
		{&m.ClickRight, ebiten.MouseButtonRight},
		{&m.ClickMiddle, ebiten.MouseButtonMiddle},
	}
}

func (m *mouseManager) MouseActions() []*Action {
	return []*Action{&m.ClickLeft, &m.ClickRight, &m.ClickMiddle}
}

var SIMULATE_TOUCH_WITH_MOUSE = false

func (m *mouseManager) position() {
	if SIMULATE_TOUCH_WITH_MOUSE && !m.ClickLeft.IsPressed {
		// When touching, the position updates only when pressing down,
		// obviously. Can't track when the finger isn't touching anywhere.
		return
	}

	m.ScreenPos.setTo(ebiten.CursorPosition())

	// This fix is an important quirk of touches.
	if SIMULATE_TOUCH_WITH_MOUSE && m.ClickLeft.IsJustPressed {
		m.ScreenPos.PrevScreenX, m.ScreenPos.PrevScreenY = m.ScreenX, m.ScreenY
	}
}

//

type ScreenPos struct {
	ScreenX, ScreenY         int
	PrevScreenX, PrevScreenY int
}

func (m *ScreenPos) setTo(x, y int) { // Can take func() (x, y int) !!!
	m.PrevScreenX, m.PrevScreenY = m.ScreenX, m.ScreenY
	m.ScreenX, m.ScreenY = x, y
}
