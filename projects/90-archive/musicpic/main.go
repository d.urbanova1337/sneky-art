package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os/exec"
	"strings"

	"os"
)

const (
	SRC = "./test.csv"
)

func readCsvFile(filepath string) [][]string {
	f, err := os.Open(filepath)
	if err != nil {
		log.Fatal("unable to read file "+filepath, err)
	}
	defer f.Close()

	reader := csv.NewReader(f)
	records, err := reader.ReadAll()
	if err != nil {
		log.Fatal("unable to parse "+filepath, err)
	}

	return records
}

func writeCsvFile(newRecord [][]string) {
	// We read the already-existing records
	// and append the new record
	// Maybe implement a backup thingy later
	records := append(readCsvFile(SRC), newRecord...)

	// Idk what's up w the additional args - if I use os.Open,
	// it throws an (allegedly Linux-specific) error
	// https://stackoverflow.com/questions/33851692/golang-bad-file-descriptor
	file, err := os.OpenFile(SRC, os.O_WRONLY, os.ModeAppend)

	if err != nil {
		log.Fatalf("failed opening file: %s", err)
	}
	defer file.Close()

	writer := csv.NewWriter(file)

	// Writing line-by-line, may be useful later
	// but for now, WriteAll() is ok
	/*
		for _, record := range records {
			if err := writer.Write(record); err != nil {
				log.Fatalln("err writing to csv: ", err)
			}
		}

		writer.Flush()
	*/
	writer.WriteAll(records)

	if err := writer.Error(); err != nil {
		log.Fatal(err)
	}
}

func getLinkByName(name string) string {
	link := ""
	records := readCsvFile(SRC)
	for _, rec := range records {
		if strings.ToLower(name) == strings.ToLower(rec[0]) {
			link = rec[1]
		}
		//fmt.Println(rec, i)
	}
	return link
}

func checkCommand(command string) error {
	_, err := exec.LookPath(command)
	if err != nil {
		return err
	}

	return nil
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("options: list | play | add")
		return
	}

	switch opt := os.Args[1]; opt {
	case "list":
		fmt.Println("  the list")
		musics := readCsvFile("./test.csv")
		fmt.Println(musics)
	case "play":
		music := os.Args[2]
		fmt.Printf("   playing %s ...\n", music)

		link := getLinkByName(music)
		prog := "ytfzf"
		//cmd := exec.Command("kitty", "-e", prog, "-am", link)
		cmd := exec.Command(prog, "-am", link)
		err := checkCommand(prog)
		if err != nil {
			panic(err)
		}

		//fmt.Println(link)
		//cmd.Start()
		fmt.Println(cmd.String())
		out, err := cmd.Output()
		if err != nil {
			panic(err)
		}

		fmt.Println(string(out))

		// use goroutine waiting, manage process
		// this is important, otherwise the process becomes in S mode
		go func() {
			err = cmd.Wait()
			fmt.Printf("Command finished with error: %v", err)
		}()

	case "add":
		fmt.Printf("\nNum of args: %v/4\n\n", len(os.Args)-1)

		link := os.Args[2]
		name := "music"
		if len(os.Args) > 3 {
			name = strings.ToLower(os.Args[3])
		}

		tags := ""
		if len(os.Args) > 4 {
			tags = os.Args[4]
		}

		if !strings.HasPrefix(link, "http") {
			fmt.Printf("%s isn't a link\n", link)
			return
		}
		rec := [][]string{
			{name, link, tags},
		}

		writeCsvFile(rec)
	}
}
