# 日 |　Day 1　| 13/03
- basic reading of csv
- shell argument handling (list, add, ...)

# 月 |　Day 2　| 14/03
- can now write to CSV
- can now play a named song with ytfzf

- [ ] fuzzy name
- [ ] nicknames?
- [ ] better error handling and reporting
- [ ] func sanitize csv in case of incompatible additions
- [ ] backup of csv

# 木　| Day 3? | 17/03
- ytfzf now shows its output in a new kitty window

- [x] display ytfzf output
- [ ] epic rofi handling
- [ ] leave the kitty win in the background, size it smol
- [ ] get rid of the random goroutine stuff i tried
- [ ] 2broradio tracker????!