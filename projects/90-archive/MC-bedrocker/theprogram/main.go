package main

import (
	_ "embed"
	"encoding/json"

	"flag"
	log "fmt"
	"strings"

	"io/fs"
	"os"
	"path/filepath"
)

func main() {
	dir := flag.String(
		"dir", ".",
		"directory of resource pack files -- will modify files in-place")
	preview := flag.Bool(
		"preview", false,
		"don't do anything -- only log what would be done")
	flag.Parse()
	flag.Usage() // just always print it, I dunno

	//

	if false { // debug
		log.Printf("\n\n# -- Blocks: \n%s\n# -- Items: \n%s\n",
			prettyMap(Data.Blocks), prettyMap(Data.Items))
	}

	log.Printf("Directory: %s\n", *dir)
	log.Printf("Mapping length :: %d\n", len(Data.Blocks)+len(Data.Items))

	renames := grabRenameOperations(*dir, Data)
	log.Printf("Renames to be done in the directory :: %d\n", len(renames))

	if *preview {
		log.Print("preview mode, printing renames only\n")
		log.Print(prettyRenames(renames))
		log.Print("preview mode, no changes have been made\n")
		return
	}

	for _, ren := range renames {
		log.Print(prettyRenames([]rename{ren}))
		err := os.Rename(ren.From, ren.To)
		// NOTE:  Doing joins creates double-dir -- the walk function has the dirnames
		//   as they should be anyway -- no need to touch them after they are grabbed.
		// filepath.Join(*dir, ren.From),
		// filepath.Join(*dir, ren.To))

		if err != nil {
			log.Printf("ERR renaming: %v", err)
			panic(err)
		}
	}

}

//
//  Reading data from the translation map and reversing the mapping
//

type jdata struct {
	// NOTE:  I'm not sure if the distinction of the two is needed, but just
	//   in case, I will respect it and do the proper thing given the data.
	Blocks map[string]string
	Items  map[string]string
}

//go:embed data.json
var DataJson []byte

var Data jdata = (func(bs []byte) jdata {
	var d jdata
	err := json.Unmarshal(bs, &d)
	if err != nil {
		log.Printf("ERR data json parse: %v\n", err)
		panic(err)
	}
	d.Blocks = reverse(d.Blocks)
	d.Items = reverse(d.Items)
	return d
})(DataJson)

func reverse(in map[string]string) (out map[string]string) {
	out = make(map[string]string) // prevent nilmap
	for key, value := range in {
		if oldkey, doubleassign := out[value]; doubleassign {
			log.Printf("FIXME :: 1- %-24s 2- %-29s VAL-- %-24s\n", key, oldkey, value)
		}
		out[value] = key
	}
	return out
}

//

func prettyMap(m map[string]string) string {
	var sb strings.Builder
	for from, to := range m {
		sb.WriteString(from)
		sb.WriteString(" -> ")
		sb.WriteString(to)
		sb.WriteString("\n")
	}
	return sb.String()
}
func prettyRenames(sl []rename) string {
	var sb strings.Builder
	for _, ren := range sl {
		from, to := ren.From, ren.To
		sb.WriteString("\nFROM : ")
		sb.WriteString(from)
		sb.WriteString("\n TO  : ")
		sb.WriteString(to)
		sb.WriteString("\n")
	}
	return sb.String()
}

//
//  Reading the asset png files recursively and renaming any that match the mapping
//

type rename struct{ From, To string }

func grabRenameOperations(dir string, d jdata) (ops []rename) {
	try := func(path string, m map[string]string) (matches bool, r rename) {
		base := filepath.Base(path)
		ext := filepath.Ext(base) // should include dot
		baseNoExt := base[:len(base)-len(ext)]

		if VALUE, ok := m[baseNoExt]; !ok {
			return // zero-values
		} else {
			return true, rename{
				From: path,
				To:   filepath.Join(filepath.Dir(path), VALUE+ext),
			}
		}
	}
	filepath.Walk(dir, func(path string, info fs.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		var matches bool
		var rename rename
		switch {
		case strings.Index(path, "/blocks/") > 0 || strings.Index(path, "/block/") > 0:
			matches, rename = try(path, d.Blocks)
		case strings.Index(path, "/items/") > 0 || strings.Index(path, "/item/") > 0:
			matches, rename = try(path, d.Items)
		}
		if matches {
			ops = append(ops, rename)
		}
		return nil
	})
	return ops
}
