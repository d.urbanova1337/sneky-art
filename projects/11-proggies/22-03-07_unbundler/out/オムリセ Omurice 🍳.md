# オムリセ Omurice 🍳
INGREDIENTS
½ onion, fine
1 chicken, 1 cm pieces
1 Tbsp oil
½ cup frozen mixed vegetables (defrosted)
salt
black pepper
1½ cups cooked Japanese short-grain rice
1 Tbsp ketchup (and more for decoration)
1 tsp soy sauce

For omelette:
2 large egg (50 g w/o shell) (separated)
2 Tbsp milk (separated)
2 Tbsp extra-virgin olive oil (separated)
6 Tbsp sharp cheddar cheese (or any kind)

https://www.justonecookbook.com/omurice-japanese-omelette-rice

# Saute -> Mix and remove from pan
onion

chicken

vegetables
salt & pepper

rice

ketchup
soy sauce

# Two portions:
## Whisk
1 egg
1 Tbsp milk

## Fry 卵焼き style
egg mixture

## Place on top
3 Tbsp cheese
1/2 fried rice on top of the omelette.

## Fold sides of omelette toward the middle
## Move omurice to edge of pan
## Flip the pan and move omurice to the plate
## Shape with a paper towel
## Drizzle ketchup