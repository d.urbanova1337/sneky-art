# Tandoori chicken 🍗
https://www.youtube.com/watch?v=GcWYXQ5vILs&t=0s
## Spice mix
> toast on pan till fragrant then grind up
1 T cumin seeds
1 T coriander seeds
4 cardamom pods
1/2 t black peppercorns
1 t fenugreek seeds
1/2 cinnamon stick

## Meat
2,2 kg of chicken legs
salt
2 T combination of cayenne pepper [hot] and paprika [not hot]
juice of 1 lemon
70 g full-fat Greek yogurt
2-3 tablespoons ginger-garlic paste

## Veg side
1 large white onion
1-2 green bell peppers
salt
high-heat oil
at least one lemon wedge per person

> 〜〜〜〜〜〜〜〜〜〜〜
score the chicken pieces with several deep cuts each
into a large glass or stainless-steel bowl
Scatter the pieces with a good coating of salt, followed by the masala (you probably won't need all of it) and the ginger-garlic paste. Squeeze in the lemon juice, put in the yogurt and the food coloring (if using). 
Mix
Cover and marinate it in the fridge for up to 24 hours

IF BROILING, put an oven-safe cooling rack onto a sheet pan and lay the chicken pieces on the rack. Get your broiler as hot as it gets, and put the chicken under it, as close as you can get the pieces without touching the element. Broiled until charred, about 10 minutes. Take the pan out and flip the pieces. Broil the second side until charred, about 10 minutes. If the drippings on the bottom of the sheet pan start to burn and smoke, pour some water into the pan. After the second side is charred, take the chicken's temperature. Internal temperature should be at least 170 F. If the chicken needs more cooking, turn the broiler off and the oven on 350 F, and put the chicken back in on a middle rack until done.

For the vegetable side, peel the onion whole, cut it into thick latitudinal slices and push to separate the pieces into individual rings. Cut the top of the pepper, reach in and tear out the seeds, then cut into thick latitudinal slices. Preheat a wide pan on high heat and coat the bottom with oil. When the oil is smoking, put in the veg, sprinkle it with salt, and let it sit for a moment until brown before tossing. Let the pieces cook a few minutes more and then take them out when they seem half-cooked.


Serve the chicken pieces on top of a bed of vegetables with lemon wedges on the side.