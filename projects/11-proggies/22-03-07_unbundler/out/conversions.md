# conversions
http://cafefernando.com/conversion-tables/#Cup%20-%20ml

Ingredient	Cup	Grams	Ounces
Flour	1	142	5
Granulated Sugar	1	213	7.5
Caster Sugar	1	213	7.5
Icing Sugar	1	170	6
Cornflour	1	170	6
Cocoa Powder	1	85	3
Rice	1	227	8
Fats	1	227	8
Dried Fruit	1	170	6
Dried Breadcrumbs	1	71	2.5
Rolled Oats	1	113	4
Dessicated Coconut	1	113	4
Walnuts (Shelled)	1	113	4
Almonds	1	142	5
Glacé Cherries	1	198	7
Lentils	1	227	8



1 cup = 16 tablespoons = 48 teaspoons = 240 ml
3/4 cup = 12 tablespoons = 36 teaspoons = 180 ml
2/3 cup = 11 tablespoons = 32 teaspoons = 160 ml
1/2 cup = 8 tablespoons = 24 teaspoons = 120 ml
1/3 cup = 5 tablespoons = 16 teaspoons = 80 ml
1/4 cup = 4 tablespoons = 12 teaspoons = 60 ml
1 tablespoon = 15 ml
1 teaspoon = 5 ml



1/4 cup of Butter

57 grams

1/3 cup of Butter

76 grams

1/2 cup of Butter

113 grams

1 cup of Butter

227 grams