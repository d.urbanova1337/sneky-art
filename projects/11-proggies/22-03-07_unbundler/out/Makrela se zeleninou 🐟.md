# Makrela se zeleninou 🐟
200g potatoes, thinly sliced
~150g (5oz) green beans, topped and tailed~
1 tbsp olive oil
4 mackerel fillets
½ bunch spring onions
~1 tbsp capers~
1 tbsp wholegrain mustard
1 lemon, juice only

Boil the kettle. Put the sliced potatoes and green beans in a saucepan and pour over the boiling water. Cover, then bring back to the boil and cook for a further 4-5 minutes.

Heat the oil in a frying pan over a high heat. Season both sides of the mackerel and put the fillets skin down in the pan. Cook for 2-3 minutes, then turn over and cook for another 1-2 minutes. Meanwhile, chop the spring onions and capers.

Drain the potatoes and beans and put in a mixing bowl together with the onions, capers, oil and mustard. Mix well to cover the potatoes and beans completely. Cut the lemon in half and squeeze over the juice. Season and serve with the mackerel








- papriky
- rajčata 
- šalotky

Tesco recipe! 
https://realfood.tesco.com/recipes/roast-mackerel-with-bay-leaves-and-lemon.html

maybe
https://www.toprecepty.cz/recept/47351-pecena-makrela-se-zeleninou/

Papriky omyjeme, zbavíme semínek a nakrájíme je na kousky. 
Rajčata spaříme horkou vodou, oloupeme je a nakrájíme na kostky. 
Šalotky oloupeme a nakrájíme na čtvrtky.
Makrely vypláchneme a osušíme. Uvnitř a na povrchu je osolíme a opepříme a naskládáme je na plech vytřený olejem. 
Vedle makrel naskládáme připravenou zeleninu, kterou také osolíme a opepříme.

Vše vložíme do trouby vyhřáté na 200 °C a pečeme asi 20 minut (podle velikosti makrel). 
Zeleninu během pečení občas promícháme a ke konci ji posypeme oreganem. 

Pečené makrely rozdělíme na talíře. 
Zeleninu promícháme s vypečenou šťávou z makrel a přidáme na talíře k rybám.