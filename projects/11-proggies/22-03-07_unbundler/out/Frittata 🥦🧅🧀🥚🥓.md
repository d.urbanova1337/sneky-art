# Frittata 🥦🧅🧀🥚🥓
some type of onion
(cured) pork product
green veggie
fresh herb

### Slowfry in order, nonstick
onion
thin, cubed meat

veggie

- right before the egg
herb

### Pour and stir
3 eggs/person
milk glug
spices

### Sprinkle on top
gouda, grated

### Optional broiling on low till cheese brown