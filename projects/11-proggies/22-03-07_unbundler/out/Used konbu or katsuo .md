# Used konbu or katsuo 
#Tsukudani (salad thing)
https://www.justonecookbook.com/simmered-kombu-tsukudani/#wprm-recipe-container-81821

55 g used kombu
½ tsp sesame seeds

###Seasonings
240 ml water
1 Tbsp sake
1 Tbsp mirin
1 tsp rice vinegar
2 Tbsp soy sauce
1 tsp sugar (add more if you prefer the sweet taste)
½ tsp katsuobushi (dried bonito flakes)
1 dried red chili pepper

###Instructions
Gather all the ingredients.
Cut the kombu into thin strips.
Remove the seeds from the dried red chili pepper and cut the pepper into thin rounds.

Transfer the kombu into a medium saucepan. Add water, mirin, and sake.
Add rice vinegar, soy sauce, sugar, and katsuobushi.

Add the red chili pepper and bring the liquid to a boil over medium heat.
Once boiling, reduce heat to low and simmer until the liquid is almost evaporated, about 20-25 minutes. If kombu is still not tender, add water and continue to cook.
Sprinkle sesame seeds and ready to serve.

###To Store
Keep the Tsukudani in the refrigerate and consume within 2 weeks.

#Furikake (rice seasoning)
https://www.justonecookbook.com/homemade-furikake-rice-seasoning/
15 g reserved kombu
30 g reserved katsuobushi
1 Tbsp toasted white sesame seeds
2 tsp toasted black sesame seeds
nori seaweed

###Seasonings
1 tsp sugar
2 tsp soy sauce
¼ tsp kosher/sea salt

###Instructions 
Gather all the ingredients. Make sure the kombu and katsuobushi are well drained.
Cut kombu into small pieces. You can cut katsuobushi into smaller pieces if you prefer.
Put kombu and katsuobushi in a saucepan and cook on medium-low heat until katsuobushi becomes dry and separated from each other.

Add sugar, salt, and soy sauce.
Cook on medium-low heat until the liquid is completely evaporated, and katsuobushi is dehydrated and crispy.
Transfer the furikake to a tray or plate and let cool. Once it’s cooled, you can add toasted/roasted sesame seeds and nori seaweed.
Put in a mason jar or airtight container and enjoy sprinkling over steamed rice and popcorn! You can refrigerate for up to 2 weeks and freeze for up to a month.