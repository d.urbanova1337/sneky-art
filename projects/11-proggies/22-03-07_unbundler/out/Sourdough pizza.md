# Sourdough pizza
[link](https://littlespoonfarm.com/sourdough-pizza-crust-recipe/#wprm-recipe-container-232)
> makes 4 crusts
(100 g) sourdough starter discard
(30 g) olive oil
(350 g) water


(50 g) whole wheat flour
(450 g) all-purpose flour
(10 g) fine sea salt

The night before
Add the pizza crust ingredients to a large mixing bowl and use your hands to mix until they are fully incorporated. Cover the bowl and allow the dough to ferment at room temperature overnight.

The next morning
Perform a set of stretch and folds. Wet your hand with water to prevent the dough from sticking. While the dough is still in the bowl, gently pull one side of the dough up and over itself. Turn the bowl and repeat this on all sides of the dough until you turned the bowl full circle.

Cover the bowl and place in the fridge, up to 36 hours until ready to bake. (The dough can be used at this point to make your pizza, the cold ferment is optional.)

### then just do the Ragusea pan