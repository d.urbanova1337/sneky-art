# Tikka sauce
1 large onion

2 T garam masala
1 T Kashmiri chili powder

2-3 T ginger-garlic paste
2-3 T tomato paste 

800g can crushed tomatoes

2-3 pounds (raw weight) cooked tandoori chicken (half the above recipe)

1/2-1 cup heavy cream
1-2 tablespoons sugar
salt
water

oil or ghee
cilantro for garnish

https://www.youtube.com/watch?v=gstyp2ZgZ1s&list=PLWDQtIyZRZu0YD4AdIlLzGwSut4r3Rdtm&index=10

Put some oil or ghee into the large pan, then cook the *onions* until they're starting to brown, stirring constantly. 

Reduce the heat to medium, then stir in the *masala,* the *chili* powder, the tomato *paste* and the ginger-garlic *paste* and fry until you're scared it's going to burn. 

Dump in the *tomatoes* and deglaze. Reduce the sauce until it gets thick and starts to caramelize, stirring constantly. 

Stir in the *cream,* then stir in enough *water* to give you a smooth, silky texture — maybe 2 cups. Stir in the *sugar,* then season with *salt* to taste. 

If you want to, strain the sauce and discard the solids. Stir in the chicken, let it heat for a sec, then put it on a plate with some rice, and garnish with cilantro leaves.