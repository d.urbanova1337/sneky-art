# Liverish meatballs 
ground beef	700 g
beef liver	200 g
bacon		200 g

egg		1
shallot/onion	1
(coconut)cream	60 ml

pepper		1 pinch
nutmeg		some
salt		lots

https://thenourishedcaveman.com/paleo-liver-bacon-meatballs

# The night before: 
If your liver was previously frozen, defrost it, then put it in a colander and let it drain for a minute. 

Soak in 2 tablespoons of apple cider vinegar and refrigerate over night.

# On the next day:
In a large pan, cook bacon over low heat, stirring often until done, without being too crispy.

Remove the bacon to a plate covered with a paper towel or a paper bag, to drain the excess fat, reserving the rendered lard in the pan.

In the meantime whisk the egg lightly with the cream and the spices.

In a food processor reduce the cooled bacon to rough crumbles. 
Now add the liver and process until it almost turns liquid. 
Next add the ground beef, onions, egg mixture, cayenne, and a bit more sea salt and pepper.

Process very lightly, just enough to blend all the ingredients, if you like a finer texture you can process longer, but the end meatballs will be denser and not as soft.

Form meatballs.

Brown meatballs on both sides, approximately 2-3 minutes per side.
Cook with lid for +-10 min on a low flame.

You can serve them with a mixed green salad or with tomato sauce on top of noodles.


/Omg, add 1 cup of shredded coconut to this recipe! I also added a bit of garlic and omitted onion as I’m intolerant. I had 2lb of beef. I might try 2 cups of coconut next time. It also holds the meat mixture together well./

/I soaked it in organic half & half overnight./
/I transferred them into a saucepan with Raos Marinara to finish cooking./

/The bacon definitely helps to balance out the liver!/