# Asian slaw

3 cups green cabbage or Chinese cabbage , shredded
▢3 cups red cabbage , shredded
▢2 large carrots , julienned
▢3 cups bean sprouts
▢3 green onions , finely sliced on the diagonal
▢1/2 cup coriander / cilantro leaves
▢1/2 cup mint leaves
▢1/4 cup Asian Fried Shallots (optional), to garnish (Note 1)

ASIAN DRESSING
▢3 tbsp rice vinegar (Note 2)
▢3 tbsp soy sauce (Note 3)
▢2 tsp fish sauce , or more soy (Note 4)
▢2 tbsp lime juice , or more rice vinegar (Note 5)
▢3 tbsp peanut oil (Note 6)
▢1 1/2 tbsp sugar , any type
▢1/2 tsp birds eye or other red chilli , finely minced (optional)
▢2 garlic cloves , minced


Instructions
Combine the dressing ingredients in a jar and shake well.

Combine the salad ingredients, except the Asian Fried Shallots.

Pour over dressing and toss to combine.

Garnish with Asian Fried Shallots. Serve!

https://www.recipetineats.com/asian-slaw/
##Recipe Notes:
Crunchy Fried Shallots - crispy fried shallot pieces, great garnish for Asian salads. Found in the Asian section of supermarket but cheaper at Asian stores!

Rice vinegar - can sub with cider vinegar or white wine vinegar.

Soy sauce - ordinary all purpose or light soy. Not dark soy.

Fish sauce - sub with more soy.

Lime juice - Sub with more rice vinegar.

Oil - sub with other neutral flavoured oil.

Storage - keep dressing separate from salad. Will keep for 3 to 4 days (depending on freshness of beansprouts