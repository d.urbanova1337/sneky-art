# Zucchini 🥒
##Fritters
### Grate and squeeze liquid out
- zucchini, 400 g
- salt, 1 tsp

### Mix all
- onion, grated, 1
- eggs, 2
- Parmesan, ½ cup /or/ hard cheese /or/ flour (3 TBSP?)
- flour, ¼ cup
- pepper & other spices, 1 tsp+

### Make patties and fry

https://wellnessmama.com/5649/zucchini-fritters/

## Creamything
### Saute, 5-7 mins
- zucchini, bite sized, 400 g

### Stir in and serve
- basil, 1 tbsp
- lemon zest, 2 tsp
- salt, ¼ tsp
- heavy cream, 100 ml /or/ coconut milk

https://wellnessmama.com/417685/lemon-tarragon-zucchini/

## Or just saute 'em

# Sauce
- yogurt, 1 cup
- minced garlic, 1 clove
- lemon juice, 2 tbsp
- salt, ¼ tsp
- pepper
- some chopped herbs