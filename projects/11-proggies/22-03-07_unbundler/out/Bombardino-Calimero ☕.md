# Bombardino-Calimero ☕
1 : 1 : 1
# Heat and mix
- rum+vanilla
- eggnog

# Mix
- coffee

# Top
- whipped cream
- nutmeg


https://www.youtube.com/watch?v=2ZD9ME08Cw8
Recipe:
2oz Brandy
2oz Advocaat
2oz Coffee
Garnish Whipped Cream & Fresh Nutmeg (optional)

Heat mug by filling with boiling water. Brew Coffee. Warm Brandy and Advocaat in a pot. Combine Coffee, Brandy and Advocaat and stir. Dump hot water from mug. Add cocktail and serve. (Optionally, top with dollop of Whipped Cream and freshly grated Nutmeg.)