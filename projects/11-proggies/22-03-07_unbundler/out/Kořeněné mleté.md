# Kořeněné mleté
Na ghí másle si lehce orestujte koření *(mletý římský kmín,* *kurkuma,* *hořčičná semínka)* s nastrouhaným zázvorem. 
Jakmile vám pánev provoní kuchyni, můžete přidat mleté hovězí *maso* a na slabé plátky pokrájenou větší jarní *cibulku.* 
Vše řádně promíchejte a restujte, dokud maso nezhnědne. 
Přimíchejte nasekanou *petrželku.* Ztlumte plotýnku a na mleté maso rozklepněte *vejce.* 
Průběžně můžete opatrně promíchat, dokud nejsou vejce lehce mazlavá. Na závěr ozdobte pánev druhou nasekanou cibulkou, lístky máty, dochuťte *pepřem,* *chilli* a několika lžícemi harissa pasty.

https://www.rohlik.cz/chef/353-masove-kulicky-s-koriandrovym-dipem