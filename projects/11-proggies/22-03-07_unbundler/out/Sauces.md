# Sauces
# ポン酢
###Steep in fridge, up to 1 month
- soy sauce, 1/2 cup
- citrus juice (3:1 lemon+orange), 1/2 cup
- lemon zest, 1 lemon
- mirin, 2 tbsp
- katsuobushi, 1/2 cup
- kombu, 1 pc, 6 g

# 照り焼き
### Boil -> simmer, 15 mins
1 : 1 : 1 : 1/2
- sake, 1/2 cup /or/ dry sherry /or/ Chinese rice wine
- mirin, 1/2 cup /or/  ¼ cup sake + ¼ cup water + 3 tbsp sugar
- soy sauce, 1/2 cup
- sugar, 1/4 cup

# 味噌
### Boil -> simmer, 25 mins
- miso, 1 cup
- mirin, 1 cup
- sake, 1/2 cup
- sugar, 2 tbsp

# 焼肉
### Simmer, 2 mins
- sake, 2 tbsp
- mirin, 2 tbsp
- sugar, 1 tsp
- rice vinegar, 1/2 tsp
- soy sauce, 3 tbsp
- miso, 1/2 tsp
- katsuobushi, 1/4 tsp
### Add and let sit overnight
- apple, 1/8
- toasted sesame s., 2 tsp

# ウナギ
### Boil -> simmer, 10 mins
- mirin, 1/2 cup
- sake, 3 tbsp
- sugar, 5 tbsp
- soy sauce, 1/2 cup