# Apple cabbage stew 🍎
## Saute I
- butter, 2 tbsp
- onion, 1 large
## Saute II
- garlic, 2 cloves
- cabbage, 1/2
- thyme sprigs, 8
## Boil -> Simmer, 15 min
- broth
- water, 1.5 liter
- apple cider vinegar, 3 tbsp
## Saute III
- apples, 3
- butter, 2 tbsp
## Mix and season
- salt ( 2 *hrstky* ) & pepper

[Link](http://www.geekychef.com/2018/11/apple-cabbage-stew.html)