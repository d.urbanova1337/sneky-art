# Mug cake 🧁☕
https://www.tablefortwoblog.com/the-moistest-chocolate-mug-cake/#wprm-recipe-container-19273

# Mix
`SALT`
flour		34 g
cocoa p.	2 tbsp
sugar		2 tbsp
baking p.	1/4 tsp

milk		60 g
oil			2 tbsp

# Microwave
70 sec + 30 secs if needed