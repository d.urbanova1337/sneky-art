# Bolognese sauce
# Ragusea
https://www.youtube.com/watch?v=V5WR-K0zJYs

olive oil

500 g carrots
1 large red onion

500 g chicken livers
1500 g ground meat (I like to use beef and lamb)

170 ml can tomato paste

1 bottle white wine

3× 800ml cans crushed tomatoes 
stock cube


1 T each:
 dried oregano
 dried basil
 dried thyme

 dried parsley 
 garlic powder

70 ml (4T) balsamic vinegar
pepper
salt

# BBC
https://www.bbcgoodfood.com/recipes/best-spaghetti-bolognese-recipe
*Ingredients*
1 tbsp olive oil

2 medium onions, finely chopped
2 carrots, trimmed and finely chopped
2 celery sticks, finely chopped
2 garlic cloves finely chopped

2-3 sprigs rosemary leaves picked and finely chopped

500g beef mince

*For the bolognese sauce*
2 x 400g tins plum tomatoes

small pack basil leaves picked, ¾ finely chopped and the rest left whole for garnish
1 tsp dried oregano
2 fresh bay leaves

2 tbsp tomato purée
1 beef stock cube

1 red chilli deseeded and finely chopped (optional)

125ml red wine
6 cherry tomatoes sliced in half