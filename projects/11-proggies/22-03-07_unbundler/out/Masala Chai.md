# Masala Chai
[rec](https://www.thehathicooks.com/how-to-make-best-chai-ever/)
# Masala
cloves, ginger, cinnamon, cardamom, nutmeg, and black pepper

(or finely ground)
32 g. (or less to start with) whole black peppercorn
25 g. whole dried ginger or ginger powder
10 g. cinnamon sticks
10 g. whole cardamom seeds 
1 g. whole cloves
1 g. nutmeg

# Chai
`one portion | 240 ml`
120 ml milk (not skim milk, see nerdy science note above)
120 ml water
1 to 2 tsp. sugar, or your favorite sweetener
1 tsp. loose tea leaves/1 *CHEAP* tea bag
1/8 to 1/4 tsp. chai masala depending on your spice preference, see recipe below


# Directions
Pour all ingredients into a (preferably spouted) saucepan. 
Place over medium heat. 
Allow to heat until small bubbles appear around the perimeter of the milk. 
Stir the chai, scraping the bottom to avoid scalding the milk. 
When the milk comes to a boil, turn off the heat and stir well. 
Bring to a boil once again, turn off the heat and stir well. 
Allow to steep for a few minutes. Strain carefully into a cup, and serve.

# or some others
https://minimalistbaker.com/easy-masala-chai-recipe/
https://www.feastingathome.com/authentic-masala-chai-recipe/#tasty-recipes-26588-jump-target
https://www.epicurious.com/recipes/food/views/homemade-chai-201226
https://www.bbcgoodfood.com/recipes/cassies-chai-tea
https://www.indianhealthyrecipes.com/masala-tea-chai/
https://www.realsimple.com/food-recipes/browse-all-recipes/easy-chai-tea-recipe