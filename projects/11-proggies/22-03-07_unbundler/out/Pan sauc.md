# Pan sauc
https://www.youtube.com/watch?v=5pV_LosNT1U
Fry the shallots in the pan for 30 seconds, 
then put in the wine and water. 
Scrape the pan to dissolve all the brown stuff on the bottom, 
and simmer the sauce for a few minutes. 
Turn off the heat and 
mix in the mustard. 
When the sauce isn't bubbling anymore, 
put in the butter and mix it in as it gradually melts. 
Tear in the herbs, 
grind in some pepper, 
and add some more salt if it needs it. 

Slice the pork chops, and pour any of the juice that comes out into the sauce. Pour the sauce over the sliced pork.