# Regarding phytic acid
### Proper sourdough prep
1. Combine all the flour in the recipe with the starter.
2. The souring time should be significant, 8 hours or more.
3. The temperature should be warm — warmer than room temperature.
4. The sourdough starter should be active and healthy, otherwise it can’t do the work.

http://www.westonaprice.org/health-topics/living-with-phytic-acid/
Sourdough fermentation of *whole wheat flour* for just *4h* at *33 C* led to  *-60% PA* … Another study showed almost *complete elimination* of phytic acid in whole wheat bread after *8h* of sourdough fermentation.

Daily consumption of one or two slices of genuine sourdough bread, a handful of nuts, and one serving of properly prepared oatmeal, pancakes, brown rice or beans should not pose any problems in the context of a nutrient-dense diet.


If the dough was at room temp for 4-6 hours before a 12 hour cold ferment and then rose at room temp for another 4-6 hours, would that be sufficient in breaking down the phytic acid?

 - And that’s what we do anytime we follow a recipe that asks for the dough to sit in the refrigerator. We give it time before and after at room temperature.