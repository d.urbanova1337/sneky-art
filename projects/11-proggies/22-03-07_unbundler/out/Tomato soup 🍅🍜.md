# Tomato soup 🍅🍜
# Basil
https://www.theseasonedmom.com/tomato-basil-soup/#wprm-recipe-container-76868
Ingredients
1 T olive oil
1 chopped onion
2 cloves minced garlic

2 *bay leaves*
3x 400g cans tomatoes, NOT drained
2 cups *broth*
2 T brown sugar
¼ cup chopped fresh *basil*

Salt & pepper, to taste
½ cup cream or half-and-half

Instructions
In a large pot, heat *olive oil* over medium-low heat. Add *onion* and *garlic* and sauté until tender (about 3-5 minutes)

Stir in *bay leaves.* Add *tomatoes,* *broth,* brown *sugar,* and fresh *basil.* Simmer on low for *15 minutes*

*Remove* bay leaves from pot. Use an immersion *blender*

Stir in the *cream* until well combined, *salt & pepper*

*Pour chilli oil upon*

/Don't omit the sugar. It's important to use sugar in tomato recipes in order to balance the acidity of the tomatoes. The end result is a more complex, rich tomato flavor. /


# Ragusi
https://www.youtube.com/watch?v=r4bGuZSur0E

1 large onion
1 *fennel bulb* (you could skip this, or replace it with another onion)
113g butter
pepper
30g flour
2 cans (800g) of tomatoes (quality matters a lot)
1 cup (237mL) white wine (very optional)
salt
water
sugar (if the tomatoes aren't sweet enough)
tomato paste (if the tomatoes aren tomatoey enough)

Roughly chop the onion and the fennel bulb (reserving the stalks and fronds for later).
Melt the butter in a big pot over medium heat and cook the onion and fennel in there for a few minutes until it starts to soften.
Grind in some pepper and put in the celery seeds. Stir in the flour, and cook it for a couple minutes.
Before anything browns, dump in the tomatoes. If the tomatoes are whole, you can accelerate their cooking by squishing them up.
Stir in the wine, if you're using it. Simmer for at least a half hour, stirring and scraping occasionally to keep anything from burning on the bottom.
For the chili oil garnish, put maybe 1/4 cup (60mL) of olive oil in a small pan and fill it with chili flakes. Drop in the garlic clove and heat it until the garlic just starts to sizzle. Leave it on low heat to infuse while the soup cooks.
When the soup is ready, puree it and then add salt and water to taste — you will probably need a lot of both. Consider the addition sugar or tomato paste to enhance the flavor, or maybe some vinegar if you didn't use the white wine. If you want it super smooth, use a stiff spoon to grind the soup through a sieve, discarding the vegetable fibers.
Serve the soup with a drizzle of chili oil on top and maybe some of the reserved fennel fronds.

# ふつう
[v2](https://www.vegrecipesofindia.com/tomato-soup-recipe-restaurant-style/#wprm-recipe-container-137972)
Ingredients
For The Soup
2 tablespoons oil
2 bay leaf
⅓ cup finely chopped onions or 1 medium onion
½ teaspoon finely chopped garlic or 2 to 3 small to medium garlic cloves
500 grams tomatoes or 6 to 7 medium to large tomatoes
1 cup water or low sodium vegetable broth
salt as required
*celery*
1 teaspoon raw sugar or white sugar
freshly crushed black pepper – as required

For Garnish
1 tablespoon chopped parsley or coriander leaves (cilantro)
1 to 2 tablespoons heavy cream – optional

Instructions
Cooking Tomatoes
Heat butter until it melts in a saucepan or a pot. Keep the heat to low or medium-low.
Add bay leaves and sauté for a few seconds. Add chopped onions and garlic. Stir and sauté until onions soften for about 3 to 4 minutes.
Add chopped tomatoes and salt. Mix well.

Cover pan and simmer on a low to medium-low heat until tomatoes soften or for about 8 to 10 minutes.
There is no need to add water. But do keep an eye when the tomatoes are simmering. If the liquids dry up then add a splash of water and continue cooking.
After the tomatoes have softened, remove from the heat and cool. Take out the bay leaves and discard.

Blending And Straining
Once the tomato mixture has cooled to a point that’s safe to work with, add it to a blender jar. You can also use an immersion blender.
Blend to a smooth consistency.
You have the option of straining the puree through a strainer for an even smoother soup, but this step isn’t necessary.

Simmering Further
Pour the tomato puree back into the pot, and add water and sugar. Mix and stir well.
Simmer on a low heat until the soup becomes hot but not boiling.
Season with freshly crushed black pepper and stir.
Turn off the heat and stir in, 1 to 2 tablespoons of heavy cream, depending on desired richness.
Mix well, taste, and add more salt and pepper if needed.

# Indian
[v1](https://twosleevers.com/indian-tomato-soup/#wprm-recipe-container-20085)
Ingredients
2 cans (822 g) Tomatoes
125 ml) Water
6 Garlic Cloves
6 slices *Ginger*, equivalent to 2 tablespoons minced

1 teaspoon Salt
2 teaspoons) Turmeric
1.5 teaspoon) Garam Masala, + 1.5 teaspoon garam masala for finishing
0.5 teaspoon) Cayenne Pepper

113.4 g) Butter, diced into cubes
119 g) Heavy Whipping Cream
1/4 cup (4 g) *Cilantro*, chopped