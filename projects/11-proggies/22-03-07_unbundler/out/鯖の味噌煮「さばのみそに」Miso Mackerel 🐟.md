# 鯖の味噌煮「さばのみそに」Miso Mackerel 🐟
https://www.justonecookbook.com/saba-misoni-simmered-mackerel-in-miso-sauce/
##The fish
2 fillets mackerel (saba) (13 oz, 374 g; 2 fillets from one mackerel; see the picture below)

##For the Seasoning #1
4 Tbsp water
2 Tbsp miso (I recommend using red or darker miso rather than white miso or koji miso with grainy texture)
4 Tbsp sake
2 Tbsp mirin
2 Tbsp sugar
1 knob ginger (sliced and julienned)

##For the Seasoning #2
2 Tbsp miso (I recommend using red or darker miso rather than white miso or koji miso with grainy texture)
1 tsp 


# Instructions
##To Prepare the Ingredients
Scrape off the ginger skin with a knife (or a spoon) and cut into thin slices. Keep half the slices for cooking the fish and use the other half for the next step.

Cut the other ginger slices into thin julienned strips and transfer to a plate. This is for garnish.

Cut each fillet in half, in a slanted angle (this creates more surface to absorb flavors). Make a shallow cross incision on the skin side of the thickest part of the fillets.

##To Blanch the Fish
Bring a medium pot of water to a boil. Once boiling, gently put a fillet, one at a time, for a quick blanch to remove the sliminess, smell, and impurities. 

Scoop up the fish with a fine-mesh strainer or a slotted spoon. Alternatively, you can pour boiling water over the fillets.

Quickly shock the fish in the ice bath. And repeat this blanch/ice bath process with the rest of the fillets.

Clean the fish in the ice water, removing blood or any impurities. The ice water helps firming up the flesh of the fish as well. Once clean, transfer to a plate/tray and set aside.

##To Cook the Fish
In a medium/large saucepan, add the Seasoning #1 (4 Tbsp water, 4 Tbsp sake, 2 Tbsp mirin, 2 Tbsp sugar, 2 Tbsp miso (keep the other 2 Tbsp miso for later), and ginger slices.

Turn the heat to medium and bring the sauce to a boil while mixing the ingredients.
When boiling, place mackerel in a single layer, skin side up.

Reduce heat to medium-low and put an otoshibuta (drop lid). Gently simmer for 13-15 minutes. Otoshibuta keeps the fish down and helps the sauce circulate over the fish.

Remove the otoshibuta (drop lid). Using the fine-mesh skimmer or slotted spoon, gently take one fillet out to a plate.

Now we add the Seasoning #2. Add 2 Tbsp miso and let it dissolve completely.
Put the fillet back in the sauce and drizzle 1 tsp soy sauce.

Hold the pot and tilt it around to distribute/mix the sauce with the fish (we use a very little amount of the sauce, so this is the best way to distribute and mix the sauce at the same time). Spoon the sauce over the fish a few times.

Turn off the heat and cover with the lid. Let the fish cool for 30 minutes. All Japanese simmered foods apply this "let cool" step so the ingredients can absorb the flavors without overcooking them.

##To Reheat and Serve
Uncovered and turn the heat to medium. Reheat the fish, but make sure to stay around the kitchen so you don't burn the thickened miso sauce. Spoon the sauce over the fish a few times.

Transfer the fish and drizzle the sauce.
Garnish with julienned ginger strips and optional Kinome.

##To Store
You can keep the leftovers in an airtight container and store in the refrigerator for up to 2 days.