# Beef heart steak 🥩
beef heart	4 slices, 2.5 cm thick
rsmry olOil	2 T
s&p		to taste

https://thenourishedcaveman.com/beef-heart-steak
Marinate heart in apple cider vinegar for 24 h

Take heart slices from the marinade and pat dry.

Heat a cast iron skillet with the ghee on a high flame for ⅔ minutes.

Lay meat in the skillet. Temperature should be high enough to sizzle.

Cook for 5 min on each side until nicely browned on the outside but still pink in the middle.

Drizzle with the rosemary infused olive oli.

Serve with salad of choice.