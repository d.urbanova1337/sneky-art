# Chocolate eggnog cake 🍰
springform pan, 26 cm diameter

>For the cake
250 grams butter
200 grams dark couverture chocolate
4 eggs
100 grams sugar
lemons
1 lemon juice
tablespoon
vanilla beans
2 cornstarch tablespoons
butter (for the pan)
pastry flour (for the pan)

For the eggnog frosting
125 milk milliliters
1/2 1 sugar tablespoon
1 egg yolk
1 cornstarch tablespoon
125 advocaat milliliters
1 cognac tablespoon
150 grams whipping cream


# Melt
chocolate, chopped
butter 

# Beat, mix
egg whites
lemon juice

egg yolks
sugar
lemon zest
vanilla

# Mix a lil'
yolky mix
choc mix

whites mix

cornstarch, sift

# Pan
grease with butter
dust with flour

pour cake in 

# Bake, 180, preheated, 45 min, lower rack
# Remove, cool
# Remove from pan, cool completely

# Eggnog frosting
Mix the milk, sugar, egg yolks and cornstarch in a saucepan and stir until
the cornstarch has dissolved. 

Bring the mixture
to a boil while stirring constantly. 

Once the mixture thickens, remove from heat and cool in
an ice water bath while stirring. Add the
Advocaat and mix thoroughly, then beat the
frosting until stiff.

Spread the eggnog frosting over the top of the
cooled cake. To serve, slice into pieces.

https://eatsmarter.com/recipes/chocolate-eggnog-cake/printview