# Pickled watermelon rind
https://www.justonecookbook.com/pickled-watermelon-rind/#wprm-recipe-container-100979

2 kg watermelon (roughly ¼ watermelon)
Seasonings
▢4 Tbsp rice vinegar
▢50 g sugar
▢2 Tbsp soy sauce
▢1 Tbsp sesame oil (roasted)
▢8 g toasted white sesame seeds
▢1 knob ginger (Optional: I occasionally add julienned ginger and it's delicious if you like the ginger taste)


##To Separate Watermelon Flesh and Rind
Cut the watermelon into 1-inch slices. Then cut each watermelon slice into 1-inch sticks.
Cut each of the watermelon stick crosswise so the watermelon is 1-inch cubes. Tip: To add natural sweetness, I like to keep a little bit of red flesh attach to the white/green rind of the fruit.
Cut the rind into 1-inch thick sticks, too. We now have 8 cups (1034 g or 2.3 lb) of the watermelon rind with skin.

##To Cut Watermelon Rind
Remove the tough dark green rind.
Cut the watermelon rind into the same size and shape for optimal pickling. For this batch, I used thin slices, but you can make it into cubes or other shapes. We now have 6 cups (803 g or 1.8 lb) of the watermelon rind without skin.

##To Pickle Watermelon Rind
Put the rind in a resealable bag (see the reason why I use a plastic bag in the blog post).
Let it pickled in the refrigerator at least overnight before serving.

##To Serve and Store
Enjoy within 3-4 days. The flavor will get stronger, if you keep the pickle solutions in the bag. It's up to you if you want to take out the rind and save in another container (discard the solution).