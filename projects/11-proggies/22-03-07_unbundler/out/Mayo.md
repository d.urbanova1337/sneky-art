# Mayo
# WMama
Ingredients
4 egg yolks (at room temperature)
1 TBSP lemon juice (or apple cider vinegar)
1 tsp Dijon mustard (or regular, or ½ tsp dried mustard powder)
½ tsp salt
¼ tsp pepper
½ cup olive oil
½ cup coconut oil (melted, or other healthy oil)

Instructions
Put egg yolks into blender or bowl and whisk/blend until smooth
Add lemon juice or vinegar, mustard, and spices and blend until mixed
SLOWLY add oil while blending or whisking at low speed, starting with olive oil. Start with a drop at a time until it starts to emulsify and then keep adding slowly until all oil is incorporated

# Lardy
¾ Cup lard, melted but cool
¾ Cup Olive oil

2 ea Egg yolks

1 Tbsp Mustard, Dijon
1 Tbsp Lemon, juice
To taste Salt, kosher
Water, as necessary to correct consistency

Directions:
Combine lard and olive oil in measuring Cup with spout.
Whisk to combine.
Add egg yolks, Dijon mustard, lemon juice, and a generous pinch of salt to bowl of food processor. Run processor for 5 seconds to combine. Scrape down sides of processor bowl with rubber spatula and process for an additional 5 seconds.
Scrape down sides of processor bowl again.
Ensure that the melted lard/olive oil mixture is no more than lukewarm, otherwise you will cook the egg yolks.
With processor running, slowly drizzle lard mixture into bowl in a very thin, slow and steady stream until an emulsion is formed.
Once an emulsion has formed, you can pour in the fat more quickly, stopping and scraping down sides of the processor bowl as necessary.
Add additional salt to taste and adjust consistency with water until thick, smooth, and creamy, but not waxy.
Store in refrigerator in airtight container for up to two weeks. 