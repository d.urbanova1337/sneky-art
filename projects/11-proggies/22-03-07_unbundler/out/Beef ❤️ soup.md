# Beef ❤️ soup
1 beef heart
herbs
1 1/2 gal. cold water
salt to taste
1 qt chopped veggies
small amount of macaroni, rice ot vermicello
cubes of golden brown bread, toasted

[link](http://www.grouprecipes.com/40119/beef-heart-soup-1856.html#)

Take 1 beef heart, cut off most of the fat and wash it thoroughly. 
Then put the heart into a kettle with 1 1/2 gallons of cold water and boil until tender.
Just before it is quite done add salt to taste.

Have ready a variety of finely chopped vegetables - about 1 quart - to which may be added a small quantity of either macaroni, rice, or vermicelli.

Boil all together for 1 hour.

Serve hot with cubes of golden brown toast, and you will enjoy a delicious soup.

Better satisfaction will be given if the heart is removed from the broth before adding the vegetables.

It may then be stuffed and baked, sliced for sandwiches or made into a fine hash.