# Pancakes 🥞
## Dry
`flour		190 g`
`sugar		24 g - 2 T`
`salt		1/2 t`

`b. powd	2 t`
`b. soda	1 t`

## Wet
`kvás-kun	240 g`
`eggs		2 large`
`milk 		240 ml +`
`butter/oil	42 g, melted`

# Overnight
> Thick and fluffy: 
Combine the wet and dry ingredients EXCEPT for the baking powder and baking soda. 

Whisk well, cover and chill overnight. 

The next day, sift the powders directly over the bowl and whisk again to combine. 

The texture should be thick, bubbly, and pourable. Add extra milk, 1 tbsp at a time, to thin out the texture if needed. 

Let the batter sit for at least 5 minutes to aerate; it should be nice and bubbly before using. The batter can be used cold.

> Thin and fluffy: 
Add all of the ingredients together and chill overnight. No need to add the leavening agents separately.

https://www.theclevercarrot.com/2020/05/homemade-fluffy-sourdough-pancakes/