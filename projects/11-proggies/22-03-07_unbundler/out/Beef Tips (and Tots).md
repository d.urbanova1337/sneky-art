# Beef Tips (and Tots)
https://www.youtube.com/watch?v=pknwlNVfias

1 ½ pounds beef round steak

1 teaspoon kosher salt, or to taste

½ teaspoon freshly ground black pepper

1 tablespoon vegetable oil

2 teaspoons tomato paste

2 cloves garlic, minced

4 tablespoons butter

2 tablespoons all-purpose flour

2 ¼ cups low-sodium beef broth

1 (28 ounce) package frozen bite-size potato nuggets (such as Tater Tots®)

/It’s crying for a dash of cayenne and sour cream/