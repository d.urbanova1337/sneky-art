# Warm, spiced olives 🌿

INGREDIENTS
1 1/2 teaspoons fennel seeds
strips of zest from one lemon
4 dried bay leaves
1 1/2 pounds mixed unpitted olives
1 cup olive oil
1 tablespoon paprika+cayenne

> or
mixed olives 500g with stones, drained 
olive oil 2 tbsp
lemon juice 2 tbsp
preserved lemon 1, halved, seeds and flesh scooped out and discarded, rind finely diced
fennel seeds 2 tsp

PREPARATION
Toast fennel seeds in a large skillet over medium heat, stirring occasionally, until fragrant, about 3 minutes. Add lemon zest, bay leaves, olives, oil, and Aleppo pepper and cook, stirring, until just warmed through, about 3 minutes. Transfer to a serving bowl and serve warm.