package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

const (
	SRC = "./src/Cookbundle/entries/entries.json"
	OUT = "./out"
)

type Recipe struct {
	Title   string
	Content string
}

func readJSONFile(filepath string) []Recipe {
	f, err := os.Open(filepath)
	if err != nil {
		log.Fatal("unable to read file "+filepath, err)
	}
	defer f.Close()

	var jsonInBytes, _ = ioutil.ReadAll(f)
	var recipes []Recipe

	json.Unmarshal(jsonInBytes, &recipes)

	return recipes
}

func writeRecipes(recipes []Recipe) {
	for _, recipe := range recipes {
		fmt.Println(recipe.Title)

		var filepath = OUT + "/" + recipe.Title + ".md"

		os.Create(filepath)
		file, err := os.OpenFile(filepath, os.O_WRONLY, os.ModeAppend)

		if err != nil {
			log.Fatalf("failed opening file: %s", err)
		}
		defer file.Close()

		file.WriteString("# " + recipe.Title + "\n" + recipe.Content)
	}
}

func main() {
	var unbundled = readJSONFile(SRC)
	for _, item := range unbundled {
		fmt.Println(item.Title)
	}
	writeRecipes(unbundled)
}
