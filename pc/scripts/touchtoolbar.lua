local dlg = Dialog("Touch Toolbar")

dlg
  :button{text="Undo",onclick=function() app.command.Undo() end}

  :button{text="<",onclick=function() app.command.GotoPreviousFrame() end}
  :button{text=">",onclick=function() app.command.GotoNextFrame() end}

  :button{
    text="+fr",     onclick=function() app.command
    .NewFrame() end}
  :button{
    text="+lay",    onclick=function() app.command
    .NewLayer() end}
  :button{
    text="[+]",     onclick=function() app.command
    .Zoom { ["action"]
    ="in" } end}
  :button{
    text="[-]",     onclick=function() app.command
    .Zoom { ["action"]
    ="out" } end}

  :show{wait=false}
