 #!/bin/sh
 
echo "prep"

# Alternative qualities: 
# -vn -c:a libopus -b:a  80k "${f}_80k.ogg"  
# -vn -c:a libopus -b:a  128k "${f}_128k.ogg"
 
for f in *.mp3; do
ffmpeg -i "$f" -vn -c:a libopus -b:a  96k "${f}_96k.ogg" 
done 

for f in *.flac; do   
ffmpeg -i "$f" -vn -c:a libopus -b:a  96k "${f}_96k.ogg"
done 

echo "------  DONE  ------"
