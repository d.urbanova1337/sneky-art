 #!/bin/sh

echo "prep"

# Alternative qualities:
# -vn -c:a libopus -b:a  80k "${f}_80k.ogg"
# -vn -c:a libopus -b:a  128k "${f}_128k.ogg"

for f in *.opus; do
ffmpeg -i "$f" -vn -ar 44100 -ac 2 -b:a  96k "${f}.mp3"
done

for f in *.wma; do
ffmpeg -i "$f" -vn -ar 44100 -ac 2 -b:a  96k "${f}.mp3"
done

for f in *.flac; do
ffmpeg -i "$f" -vn -ar 44100 -ac 2 -b:a  96k "${f}.mp3"
done

# Alternative file format catching
# for f in *.opus; do
# ffmpeg -i "$f" -vn -ar 44100 -ac 2 -b:a  96k "${f}.mp3"
# done

echo "------  DONE  ------"


# Explanation of the used arguments in this example:
#
# -i - input file
#
# -vn - Disable video, to make sure no video (including album cover image) is included if the source would be a video file
#
# -ar - Set the audio sampling frequency. For output streams it is set by default to the frequency of the corresponding input stream. For input streams this option only makes sense for audio grabbing devices and raw demuxers and is mapped to the corresponding demuxer options.
#
# -ac - Set the number of audio channels. For output streams it is set by default to the number of input audio channels. For input streams this option only makes sense for audio grabbing devices and raw demuxers and is mapped to the corresponding demuxer options. So used here to make sure it is stereo (2 channels)
#
# -b:a 192k - Converts the audio bit-rate to be exact 192 KB/s (192 kibibit per second).
