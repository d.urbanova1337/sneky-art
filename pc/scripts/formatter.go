package main

import (
	"fmt"
	"strings"

	"os"
	"os/exec"
)

// Just set this...

var (
	Layout string = bm
	Text          = `
        KC_LGUI,  KC_Q,    KC_W,    KC_F,    KC_P,    KC_B,    KC_J,    KC_L,    KC_U,    KC_Y,  LSFT(KC_NUHS),    KC_ESC,
        KC_LCTL,  KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_ENT,
        KC_LSFT,  KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,  KC_COMM, KC_DOT,  KC_UP,   KC_SLSH,
        KC_LALT, MO(3),  KC_TAB,   MO(2),   MO(1),   KC_BSPC,         KC_SPC, KC_LEFT, KC_DOWN, KC_UP, KC_RGHT
	,,,
	`
)

// --------------------------------------------------------------

func Parts(s string) []string {

	const comma = ','
	const comma_s = ","

	// NOTE:  It's just ASCII, we can for loop it boldly...
	var partEnds []int
	{ // Gathering where parts end...

		// Because the commas can be part of macro arguments,
		// we have to make sure the commas for splitting are
		// actually splitting the keys...
		parDepth := 0
		const parStart, parEnd = '(', ')'

		for i, c := range s {
			switch c {
			case parStart:
				parDepth++
			case parEnd:
				parDepth--
			case comma:
				if parDepth == 0 {
					partEnds = append(partEnds, i+1)
					// i+1 because we should split after the comma, not before it.
					// Makes it easier to reason about it later on.
				}
			}
		}
		// Putting an end on the absolute end of the string
		// for that last part that, due to C being kinda bad,
		// cannot have a trailing comma.
		partEnds = append(partEnds, len(s))
	}

	var parts []string
	{ // Splitting the string into parts, doing cleanup
		prev := 0
		var CutSet = " \t\n" + comma_s
		for _, ix := range partEnds {

			p := s[prev:ix]
			prev = ix

			p = strings.Trim(p, CutSet)
			if len(p) == 0 {
				// Due to trimming the comma, repeated whitespace and commas
				// will be taken care of here.
				continue
			}
			// Now that comma will be put back.
			p = p + comma_s

			parts = append(parts, p)
		}
		// The last part in the list cannot have the comma.
		// It's easy enough to do this right here.
		last := &parts[len(parts)-1]
		*last = strings.Trim(*last, CutSet)
	}

	return parts
}

// ----------------------------------------------------------------

var (
	corne = "" +
		".....|.....\n" +
		".....|.....\n" +
		".....|.....\n" +
		"  ...|...  "

	lily58 = "" +
		"...... | ......\n" +
		"...... | ......\n" +
		"...... | ......\n" +
		".......|.......\n" +
		"   ....|....   "

	bm = "" +
		"............\n" +
		"............\n" +
		"............\n" +
		"...........\n"
)

func layoutName(layout string) string {
	switch layout {
	case corne:
		return "Corne"
	case lily58:
		return "Lily58"
	case bm:
		return "BM40"
	}
	return "-layout name not set-"
}

const (
	Key       = "."
	Padding   = " "
	HandSepar = "|"
)

func FormatParts(keeb string, parts []string) (string, error) {

	// Remove the split -- it's just for reading. Unless I change
	// my mind; then it would be for checking if we're on left
	// or right side.
	keeb = strings.ReplaceAll(keeb, HandSepar, "")

	// Let's grab all the shit. This doesn't even touch the `parts` yet,
	// it's just a pre-compilation step.
	var layout = struct {
		lines     []string
		colWidths []int
		nKeys     int
	}{}
	{
		layout.lines = strings.Split(keeb, "\n")

		// I guess I should check all lines for errchecks, but I won't.
		// Just make sure the strings are the same, pad them out to size.
		nCols := len(layout.lines[0])
		// We can just do the width array right now.
		layout.colWidths = make([]int, nCols)

		// This one is obvious.
		layout.nKeys = strings.Count(keeb, Key)
	}

	//

	if layout.nKeys != len(parts) {
		return "", fmt.Errorf(
			"Number of keys doesn't match: %v instead of %v",
			len(parts), layout.nKeys)
	}

	// Find out the widths for every column -- we'll want to
	// make a proper table for that.
	part := 0
	for _, lline := range layout.lines {
		////fmt.Printf("-------------------\n")
		for col, lc := range lline {
			if string(lc) != Key {
				////fmt.Printf("<<````>>\n")
				continue
			}
			////fmt.Printf("<<%s>>\n", parts[part])

			this := len(parts[part])
			wid := &layout.colWidths[col]
			if this > *wid {
				*wid = this
			}

			part++
		}
		////fmt.Printf("%v\n", layout.colWidths)
	}
	////fmt.Printf("WIDTHS: %v\n", layout.colWidths)

	// Now we have to make the format string precisely for
	// the column widths and the column positions.
	// We will be later putting in the padding and inserting
	// extra elements into the formatting call.
	// The string will be reused for every line the same way.
	fmtStr := ""
	for i, w := range layout.colWidths {

		// Figure out what hand it is -- the formatting is different
		diff := i - len(layout.colWidths)/2
		if diff == 0 {
			// The gap between hands can be modified
			fmtStr += "    "
			//fmtStr += "/**/" // This would break block comments tho
		}

		// yea, those fmt strings here a bit too meta....
		if diff < 0 { // Left
			fmtStr += fmt.Sprintf("%%%ds  ", w)
		} else { // Right
			fmtStr += fmt.Sprintf("  %%-%ds", w)
		}
	}
	{ // Copy `lines` times
		allLines := ""
		for range layout.lines {
			allLines += "    " + fmtStr + "\n"
		}
		fmtStr = allLines
	}
	////fmt.Printf("<<%d<<\n%s\n>>>>\n", len(layout.lines), fmtStr)

	// Yea, this requires the weird forloop again, sadly -- just slightly
	// adjusted; enough to be very confusing.
	// Parts must be of `[]interface{}` to be used as params
	// for a Sprintf. This must be done element-by-element.

	part = 0
	var fmtArgs = make([]interface{}, 0) // len(layout.lines)*len(layout.colWidths)
	for _, lline := range layout.lines {
		for _, lc := range lline {
			if string(lc) == Key {
				fmtArgs = append(fmtArgs, parts[part])
				part++
			} else {
				fmtArgs = append(fmtArgs, "")
			}
		}
	}

	if len(fmtArgs) != len(layout.lines)*len(layout.colWidths) {
		return "", fmt.Errorf("!!!\n%d instead of %d\n!!!\n",
			len(layout.lines)*len(layout.colWidths), len(fmtArgs))
	}

	s := fmt.Sprintf(fmtStr, fmtArgs...)
	////fmt.Printf("<<<\n%s\n>>>\n", s)

	return s, nil
}

// -------------------------------------------------------------------

func main() {

	Verbose  := (len(os.Args) > 2 && os.Args[2] == "v")
	UseXClip := (len(os.Args) > 1 && os.Args[1] == "xclip")

	if UseXClip {
		if Verbose {
			fmt.Printf("USING THE SYSTEM CLIPBOARD w/ `xclip`\n")
		}
		fromClipboard, err := Clipboard_Read()
		if err != nil {
			fmt.Printf("Clipboard read err: %v\n", err)
		} else {

			Text = fromClipboard

		}
	}
	if Verbose {
		fmt.Printf("Input (xclip=%t): \n\n%s\n\n", UseXClip, Text)
	}

	ps := Parts(Text)

	if Verbose {
		fmt.Printf("Layout Used = %s: \n%s\n\n", layoutName(Layout), Layout)
	}

	Output, err := FormatParts(Layout, ps)
	if err != nil {
		fmt.Printf("[ERR]: %+v\n", err)
		return
	}

	if Verbose {
		fmt.Printf("Output (saveToClipboard=%t): \n\n%s\n", UseXClip, Output)
	}

	if UseXClip {
		Clipboard_Write(Output)
	}
	if !Verbose {
		fmt.Printf("== All's well! ==")
	}
}

// ==================================================================
// XCLIP CODE TAKEN FROM :: github.com/ZacJoffe/clipboard
// ==================================================================

// checkCommand looks sees is a given command exists on the user's system, if the returned error is nil then it exists
func checkCommand(command string) error {
	_, err := exec.LookPath(command)
	if err != nil {
		return err
	}

	return nil
}

// Clipboard_Read reads whatever is in the clipboard, and returns it as a string
func Clipboard_Read() (string, error) {
	// check to see if xclip exists
	err := checkCommand("xclip")
	if err != nil {
		return "", err
	}

	// run the command "xclip -out -selection clipboard" to get what's in the clipboard
	cmd := exec.Command("xclip", "-out", "-selection", "clipboard")
	out, err := cmd.Output()
	if err != nil {
		return "", err
	}

	// convert the output to a string, and return it
	return string(out), nil
}

// Clipboard_Write writes a given string to the clipboard
func Clipboard_Write(text string) error {
	// check to see if xclip exists
	err := checkCommand("xclip")
	if err != nil {
		return err
	}

	// create the command "xclip -in -selection clipboard"
	cmd := exec.Command("xclip", "-in", "-selection", "clipboard")

	// create new stdin pipe
	in, err := cmd.StdinPipe()
	if err != nil {
		return err
	}

	// start the command
	err = cmd.Start()
	if err != nil {
		return err
	}

	// write the given text parameter into the writer, which will act as the stdin
	in.Write([]byte(text))
	if err != nil {
		return err
	}

	// explicitly close the writer
	err = in.Close()
	if err != nil {
		return err
	}

	// wait for command to exit and stdin copying - will return nil if successful
	return cmd.Wait()
}
