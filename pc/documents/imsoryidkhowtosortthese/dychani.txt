
%
%
%  === DYCHANI ===
%
%


S čím dýchací tréninky pomohou?
-- Dušnost všech druhů a příčin
-- Anémie / chudokrevnost
-- Zmírnění následků alergií
-- Zlepšení VO2 Max (astmatici znají)
-- Lapání po dechu po fyzické aktivitě
-- Sníží celkově tep; při aktivitě i klidový tep
-- Zlepší variabilitu tepu 
-- Zničí problémy s rýmou





Každý si díky dnešnímu 'fitkovému' šílenství myslí, že jediný způsob, jak mít dobrou fyzičku, je cvičení. Mohu vám s naprostou jistotou říct, že kardio cvičení pro udržení kondice je ztráta času a úsilí.

A je škoda, že si lidé myslí, že návštěva fitka nebo pobíhání po parku je jediná cesta k 'fyzičce'...
Pokud víte //co// a //jak// trénovat, stačí překvapivě málo úsilí, může tak trénovat klidně i člověk na vozíčku a //nic// to nestojí.

Dále se tady dočtete, na čem taková fyzička //vážně// závisí. Nejlepší je, že zlepšovat kondici lze i v sedě před televizí! Zabere vám to navíc nanejvýš 15 minut denně.

(A není to nějaká pitomost jako teleshopingový vibrační pás za osm stovek, co bude 'cvičit za vás')

Tyto techniky bude zaručeně znát kdokoliv, kdo zkoušel jógu a učil ho člověk, který ví, co dělá. V asijských tradičních cvičeních je kladen velký důraz na dýchání - a z dobrého důvodu. Ovlivňuje totiž úplně všechnu oxidaci v těle, což pro laiky znamená 'pálení energie'. Jinými slovy zdokonaluje metabolismus a můžete díky tomu zlepšit i hubnutí. Ale jen zlepšit - jen dýcháním nikdo bohužel nezhubne.


### Co nás nutí dýchat? 

Ze začátku si řekneme jak to celé funguje, abyste věděli, o co se při dýchání snažit. Pobud budete chápat biomechanismy, sami se můžete rozhodnout, jaké cvičení je pro vás nejvýhodnější.  

Schválně - co si myslíte, že vás nutí se nadechnout? ...zdá se to jako jednoduchá otázka s jednoduchou odpovědí. 'Protože mi dochází kyslík! ...ne? ...NE?!'

Bohužel, není tomu tak; je to trochu složitější. A můžeme se o tom sami přesvědčit pomocí následujícího experimentu (nebojte, nezabije vás to): 

Začněte dýchat jen nosem. Pomalu. Všímejte si, kdy se vám chce nadechovat. Až budete připraveni, nadechněte se a zadržte dech. Až začnete mít pocit, že už nemůžete držet dech dál, vydechněte a $$znovu zadržte dech$$.

Co to? Zrovna jste vydechli //ještě více kyslíku//, ale můžete dál na chvíli dech znovu držet? Věřte mi, že jste své tělo neoblbnuli - děje se tu velmi zajímavá věc.

Jo a můžete vydechnout, heh. Doufám, že jste se neudusili! 

//Co tento experiment tedy dokazuje?// 

Když jsme se potřebovali nadechnout, oxidací jsme přeměnili nadechnutý kyslík na oxid uhličitý, který pak držením dechu nashromáždil. Když jsme ho vydechli, získali jsme moment úlevy, protože jsme se zbavili části CO2 v těle. Množství kyslíku v těle se ale nezměnilo, ani jsme se nenadechli. 



### Dušnost je způsobena velkou koncentrací oxidu uhličitého v krvi, ne nedostatkem kyslíku

Všichni snad známe ze školy, jak funguje dýchání... ale dáme si opáčko, s trochou detailů podstatných pro nás:

Nadechnete vzduch - směs různých plynů; především dusíku (78%), kyslíku (20%), trochu oxidu uhličitého (<1%) a ostatních plynů. Molekuly kyslíku se v plicích 'nalepí' na červené krvinky a krví se skrz tepny roznesou do celého těla.

Každá buňka pak při oxidaci - přeměně cukrů //nebo// tuků na energii - přemění kyslík na oxid uhličitý. Čím větší výdej energie, tím více oxidace a tím vyšší produkce oxidu uhličitého. Zjednodušeně při oxidaci nalepí buňka na molekulu kyslíku jeden atom uhlíku.

(Pro pochopení výhod půstů{} a nízko-sacharidových diet{} je důležité i to, že oxidace cukrů vytváří více oxidu uhličitého než pálení tuku pro to samé množství energie! Jinak řečeno: při půstu nebo správné dietě můžete svoji kondici zlepšit //ještě více//!) 

Pro správnou oxidaci se v buňce prohodí v červené krvince molekuly O2 a CO2 tak, že červená krvinka plná O2 je donucena molekulou CO2, aby laskavě vystoupila, udělala místo a nechala se využít buňkou.

$$Kyslík se pouští červených krvinek jen v prostředí oxidu uhličitého.$$

Když krvinka nabere CO2, putuje žilami zpět do plic, kde krvinka oxid uhličitý ztrácí a my ho vydechneme. Při nádechu se do plic dostane nový kyslík a celý proces se opakuje. 

Většina lidí neví, že koncentrace kyslíku v krvi se málokdy dostane pod 95%. Krev je NEUSTÁLE nasycená kyslíkem, ale je práce oxidu uhličitého ho dostat ven, aby kyslík mohly využívat buňky. 


### Co tedy nutí tělo se nadechovat? Jak dýchání zlepšit? 

Biologicky, je to //kyselost naší krve// co nám způsobuje pocit se nadechnout - tělo je na to neskutečně citlivé, aby nepoškodilo kyselým nebo zásaditým prostředím své buňky. Právě CO2 dělá krev více zásaditou; to je pro tělo známkou, že je čas dostat dýcháním hodnotu pH krve dolů a dýchat.

//Nádechem// se dostane do krve víc O2 (ta 'zředí' krev) a tím se pH zvýší - je více zásaditá. //Výdechem// se pak z krve dostane ven CO2 a tím se pH sníží - je více kyselá. Tyto změny v kyselosti bere tělo jako signály k dýchání. Pro zajímavost: pH krve se pohybuje těsně mezi 7,35 a 7,45. 

$$Cílem našich cvičení je donutit tělo, aby si zvyklo na změny pH krve tím, že ho budeme změnám vystavovat.$$

Reaguje na to dvěma způsoby: Za prvé si prostě zvykne nepanikařit tak brzy a začne tolerovat větší výkyvy pH. Za druhé - a nedělám si srandu - //začne vytvářet více červených krvinek//!! Tím vám vlastně pomůže víc, než prášky s železem předepisované lidem s chudokrevností - dokud samo tělo nemá pocit, že potřebuje více krvinek na přepravu, ani prášky s materiály pro stavbu ho nedonutí. 


### '...Takže se mám jako... dusit?!'

Ano. Tak trochu. Samozřejmě ne tak, že by vám bylo životu nebezpečné, ale ano.

Trénink spočívá v tom, že dýcháme co nejpomaleji a využíváme především bránicí - prsa se nám ani nehnou, zvedáme hlavně břicho při nádechu a vzduch ven netlačíme, ale necháváme břicho samo od sebe vyfouknout.

Dýcháme tak pomalu, že celé tělo po nás bude křičet, abychom dýchali rychleji. Počítejte si jak dlouho vám zabral každý nádech a zkuste časy zlepšovat. Pokud vám omylem ulítne nádech, zadržte o to víc výdech. Hrajte si s tím. Vaše bránice bude i ze začátku protestovat mírnými záškuby - to už je prosím //trošičku příliš//.

Že to zní hodně jako pocit, když běžíte na plno a nemůžete polapit dech? //Přesně tak!!// Přesně proto je to tak efektivní - jako běhání, ne-li více. Místo toho, abyste cvičením v sobě vytvářeli více oxidu uhličitého, můžete nedělat nic a místo toho se snažit ho tolik nevydechovat a dosáhnout stejné koncentrace CO2 jako při běhání.

Nebudu vám lhát, dělat to delší dobu je náročné. Ze začátku nebudete moct ani vnímat, co se děje kolem vás a budete muset být naprosto koncentrovaní, abyste to nezačali 'flákat'. Nebude vás to bavit a bude to otrava. Ujišťuji vás ale, že benefity tohoto cvičení se ukážou //už po prvních několika dnech//.

Jsem ale pro to, že lidi s nadváhou nemůže nikdo nutit do fyzického cvičení. Kdo by chtěl cvičit, když ani nevyběhne schody. Tohle je ale něco co může dělat kdekoliv kdokoliv - od obézního důchodce přes 14letého astmatika až po zaneprázdněného 25letého atleta. 

Koukáte na telku? Dýchejte. Čekáte na zastávce na autobus? Dýchejte. Musíte jít pěšky 20 minut do práce? Dýchejte. Jedete MHDčkem do města? Dýchejte. Cvičíte ráno když se vzbudíte? Dýchejte. Nemůžete usnout a nedokážete zastavit to neustálé myšlení na blbosti ve 2 ráno? Dýchejte. Sedíte na záchodě? Dýchejte.

(Tip: Tyto pomalé pohyby bránicí výborně pomáhají při zácpě nebo když chcete vynutit jít ráno ve spěchu na záchod! Lepší než kafe!)

Kdykoli se vám to hodí a nemusíte nad ničím jiným nijak přemýšlet (//...a nehrozí, že kvůli tomu někoho třeba zajedete, nebo někdo zajede vás!! Dávejte na sebe pozor!!//), zkuste se soustředit na to, abyste pomalu dýchali a abyste 'neplýtvali vzduchem' jak rád říkám.

Udělejte pomalé dýchání vaším zvykem, všímejte si jak dýchá okolí a třeba je poučte o svých nových poznatcích. Dávat si na tohle pozor se vyplatí komukoliv, nic to nestojí a výsledek je vidět téměř okamžitě.

Nevzdávejte se a hodně štěstí!! 
