import bpy
from os.path import dirname
import os

# add object funtion
def add_objects(name):
    before_data = list(bpy.data.objects)
    bpy.ops.wm.append(directory=os.path.join(dirname(os.path.dirname(__file__)), "MotionPrimitives.blend\Object") , filename=name, link=False)
    new_data = list(filter(lambda d: not d in before_data, list(bpy.data.objects)))
    appended = None if not new_data else new_data[0]
    bpy.context.view_layer.objects.active = appended
    appended.location = bpy.data.scenes['Scene'].cursor.location

# add object operator for button
class OBJECT_OT_add_panel(bpy.types.Operator):
    bl_idname = "icg.template_add_panel"
    bl_label = "Add Object"
    bl_description = ""
    bl_options = {'UNDO'}

    def execute(self, context):
        fName = os.path.splitext(bpy.context.scene.thumbnails)[0]
        add_objects(fName)
        return {'FINISHED'}
