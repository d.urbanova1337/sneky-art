import bpy
op = bpy.context.active_operator

op.align = 'WORLD'
op.location = (0.0, 0.0, 0.0)
op.rotation = (0.0, 0.0, 0.0)
op.Star = True
op.change = False
op.points = 5
op.outer_radius = 0.7599999904632568
op.innter_radius = 0.5
op.height = 0.02000001072883606
