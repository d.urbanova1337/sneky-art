# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "Motion Locale: Motion Primitives",
    "author" : "atti, @tufjo_",
    "description" : "Extra Objects for Motion Graphics",
    "blender" : (2, 80, 0),
    "version" : (1, 0, 0),
    "location" : "N-Panel",
    "warning" : "",
    "category" : "Assets"
}
import bpy
from bpy.types import Panel, EnumProperty, WindowManager, Menu
from .operators.object_add import OBJECT_OT_add_panel
from .t3dn_bip import previews
import os
from os.path import dirname
from . import auto_load

auto_load.init()

#add panel
class Motion_PT_primitives_panel(bpy.types.Panel):
    bl_idname = "Motion_PT_Panel"
    bl_label = "Motioni Primitives"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "atti"

    def draw(self, context):
        layout = self.layout
        wm = context.window_manager
        row = layout.row()
        
        # draw the my_previews window manager object
        row.template_icon_view(context.scene, "thumbnails", show_labels=True, scale=6, scale_popup=3)

        row = layout.row()
        row.operator("icg.template_add_panel",text="Add Object")

preview_collections = {}

def generate_previews():
    # Accessing info generated in the register function
    pcoll = preview_collections["thumbnail_previews"]
    image_location = pcoll.images_location
    
    enum_items = []
    
    # Generate thumbnails
    for i, image in enumerate(os.listdir(image_location)):
        if image.endswith('.bip'):
            clean = os.path.splitext(image)[0]
            filepath = os.path.join(image_location, image)
            thumb = pcoll.load(filepath, filepath, 'IMAGE')
            enum_items.append((image, clean, "", thumb.icon_id, i))
            
    return enum_items

def register():
    if not bpy.app.background:  #~~fix~~ hacky workaround to prevent blocking background renders (https://developer.blender.org/T95495) (I have no idea what causes it)
        from bpy.types import Scene
        from bpy.props import StringProperty, EnumProperty
        bpy.utils.register_class(Motion_PT_primitives_panel)
        # Create a new preview collection (upon register)
        pcoll = previews.new(max_size=(256, 256))
        pcoll.images_location = os.path.join(os.path.dirname(__file__), "thumb")
    
        # Enable access to our preview collection outside of this function
        preview_collections["thumbnail_previews"] = pcoll
    
        # This is an EnumProperty to hold all of the images
        # can save it anywhere in bpy.types.*  Just make sure the location makes sense
        bpy.types.Scene.thumbnails = EnumProperty(
        items=generate_previews(),
        default = None
            )
        auto_load.register()

def unregister():
    if not bpy.app.background: #~~fix~~ hacky workaround to prevent blocking background renders (https://developer.blender.org/T95495)
        from bpy.types import WindowManager
        bpy.utils.unregister_class(Motion_PT_primitives_panel)
        for pcoll in preview_collections.values():
            previews.remove(pcoll)
        preview_collections.clear()
        del bpy.types.Scene.thumbnails
        auto_load.unregister()
