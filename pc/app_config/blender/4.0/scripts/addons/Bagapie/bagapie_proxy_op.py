import bpy
import json
import os
import addon_utils
from bpy.types import Operator
from . presets import bagapieModifiers
from random import random

class BAGAPIE_OT_proxy_remove(Operator):
    """ Remove Bagapie Proxy modifiers """
    bl_idname = "bagapie.proxy_remove"
    bl_label = 'Remove Bagapie Proxy'
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        o = context.object

        return (
            o is not None and 
            o.type == 'MESH'
        )
    
    index: bpy.props.IntProperty(default=0)
    
    def execute(self, context):
        
        obj = context.object
        val = json.loads(obj.bagapieList[self.index]['val'])
        try:
            modifiers = val['modifiers']
            print(modifiers)
            for mod in modifiers:
                obj.modifiers.remove(obj.modifiers[mod])
        except:
            print("Proxy modifier is missing")
            
        context.object.bagapieList.remove(self.index)

        return {'FINISHED'}


class BAGAPIE_OT_proxy(Operator):
    """Create convex hull visible only in the viewport"""
    bl_idname = 'bagapie.proxy'
    bl_label = bagapieModifiers['proxy']['label']
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        o = context.object
        l = ['MESH','CURVE']
        return (
            o is not None and 
            o.type in l
        )

    disable_proxy:  bpy.props.BoolProperty(default=False)
    found_proxy:  bpy.props.BoolProperty(default=False)
    
    def invoke(self, context, event):
        self.found_proxy = False
        wm = context.window_manager
        targets = bpy.context.selected_objects
        for target in targets:
            if target.type  in ['MESH','CURVE']:
                for modifier in target.modifiers:
                    if modifier.name.startswith("BagaPie_Proxy"):
                        if modifier.show_viewport == True:
                            print('Proxy Modifier already present on : '+ target.name)
                            self.found_proxy = True
                        else :
                            self.disable_proxy = False
                    elif modifier.type == 'NODES':
                        if modifier.node_group.name.startswith("BagaPie_Proxy") and modifier.show_viewport == True:
                            print('Proxy Modifier already present on : '+ target.name)
                            self.found_proxy = True
                        else :
                            self.disable_proxy = False
        if self.found_proxy == True:
            return wm.invoke_props_dialog(self)
        else:
            return self.execute(context)

                        
    def draw(self, context):
        layout = self.layout
        if self.found_proxy == True:
            layout.label(text = "Proxy found on one or more of the selected objects")
            layout.prop(self, 'disable_proxy', text = "Disable proxy on selected objects ?")

    def execute(self, context):
        targets = bpy.context.selected_objects
        for target in targets:
            if self.disable_proxy == True:
                if target.type in ['MESH','CURVE']:
                    for modifier in target.modifiers:
                        if modifier.name.startswith("BagaPie_Proxy"):
                            modifier.show_viewport = False
                        elif modifier.type == 'NODES':
                            if modifier.node_group.name.startswith("BagaPie_Proxy"):
                                modifier.show_viewport = False

            elif target.type in ['MESH','CURVE']:
                skip = False
                for modifier in target.modifiers:
                    if modifier.name.startswith("BagaPie_Proxy"):
                        print('Proxy Modifier already present on : '+ target.name)
                        skip = True
                        modifier.show_viewport = True
                    elif modifier.type == 'NODES':
                        if modifier.node_group.name.startswith("BagaPie_Proxy"):
                            print('Proxy Modifier already present on : '+ target.name)
                            skip = True
                            modifier.show_viewport = True

                if skip == True:
                    continue

                new = bpy.data.objects[target.name].modifiers.new

                nodegroup = "BagaPie_Proxy" # GROUP NAME

                modifier = new(name=nodegroup, type='NODES')
                Add_NodeGroup(self,context,modifier, nodegroup)
                target.modifiers[nodegroup].show_render = False

                mat_proxy = bpy.data.materials.new(name="BagaPie_Proxy")
                mat_proxy.diffuse_color = (random(), random(), random(), 1)

                #Assign material        
                modifier["Input_6"] = mat_proxy
                
                val = {
                    'name': 'proxy', # MODIFIER TYPE
                    'modifiers':[
                        modifier.name, #Modifier Name
                    ]
                }

                item = target.bagapieList.add()
                item.val = json.dumps(val)
        
        return {'FINISHED'}


###################################################################################
# ADD NODEGROUP TO THE MODIFIER
###################################################################################
def Add_NodeGroup(self,context,modifier, nodegroup_name):
    try:
        modifier.node_group = bpy.data.node_groups[nodegroup_name]
    except:
        Import_Nodes(self,context,nodegroup_name)
        modifier.node_group = bpy.data.node_groups[nodegroup_name]


###################################################################################
# IMPORT NODE GROUP
###################################################################################
def Import_Nodes(self,context,nodes_name):

    for mod in addon_utils.modules():
        if mod.bl_info['name'] == "BagaPie Modifier":
            filepath = mod.__file__
            file_path = filepath.replace("__init__.py","BagaPie_Nodes.blend")
        else:
            pass
    inner_path = "NodeTree"
    # file_path = r"C:\Users\antoi\Desktop\BagaPie Archive\Dev\Bagapie\BagaPie_Nodes.blend"

    bpy.ops.wm.append(
        filepath=os.path.join(file_path, inner_path, nodes_name),
        directory=os.path.join(file_path, inner_path),
        filename=nodes_name
        )
    
    return {'FINISHED'}


classes = [
    BAGAPIE_OT_proxy_remove,
    BAGAPIE_OT_proxy,
]